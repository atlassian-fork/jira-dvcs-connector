package com.atlassian.jira.plugins.dvcs.spi.bitbucket.linker;

import com.atlassian.jira.plugins.dvcs.model.Repository;

import java.util.Set;


public interface BitbucketLinker {

    /**
     * Configures bitbucket repository by adding links to Projects in this JIRA instance.
     * It will only add link to a Project if an issue from that Project is referenced in any commit message
     * in target Bitbucket Repository.
     */
    void linkRepository(Repository repository, Set<String> projectsInChangesets);

    /**
     * Removes all links that were previously configured by {@link #linkRepository(Repository, Set)}.
     */
    void unlinkRepository(Repository repository);
}
