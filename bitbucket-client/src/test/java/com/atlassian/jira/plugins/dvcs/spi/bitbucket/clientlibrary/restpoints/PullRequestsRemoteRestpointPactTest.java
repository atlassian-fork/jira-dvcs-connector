package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints;

import au.com.dius.pact.consumer.ConsumerPactBuilder;
import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactDslJsonBody;
import au.com.dius.pact.consumer.PactProviderRule;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.model.PactFragment;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fusion.aci.api.service.ACIJwtService;
import com.atlassian.fusion.aci.api.service.ACIRegistrationService;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketPullRequest;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.BitbucketRequestException;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.HttpClientProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.auth.ACIJwtAuthProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.auth.NoAuthAuthProvider;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * PACT tests for the {@link PullRequestRemoteRestpoint}
 */
public class PullRequestsRemoteRestpointPactTest {
    private static final String ACCOUNT_NAME = "dvcspact";
    private static final String REPO_NAME = "pact_test";
    private static final String PR_ID_EXAMPLE = "1";
    private static final String NON_EXISTENT_ACCOUNT_NAME = "non_existent";

    private static final String ACCOUNT_PATTERN = "[a-zA-Z0-9{}\\-\\._]+";
    private static final String REPOSITORY_SLUG_PATTERN = ACCOUNT_PATTERN;
    private static final String PR_ID_PATTERN = ACCOUNT_PATTERN;

    private static final String PRS_API_PATTERN = "(/!?api)?/2.0/repositories/" + ACCOUNT_PATTERN + "/" + REPOSITORY_SLUG_PATTERN + "/pullrequests/" + PR_ID_PATTERN;
    private static final String PRS_API_EXAMPLE = "/api/2.0/repositories/" + ACCOUNT_NAME + "/" + REPO_NAME + "/pullrequests/" + PR_ID_EXAMPLE;
    private static final String PRS_API_NON_EXISTENT_EXAMPLE = "/api/2.0/repositories/" + NON_EXISTENT_ACCOUNT_NAME + "/" + REPO_NAME + "/pullrequests/" + PR_ID_EXAMPLE;
    private final HttpClientProvider httpClientProvider = new HttpClientProvider();
    @Rule
    public PactProviderRule pactProvider = new PactProviderRule("bitbucket", this);
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    private ACIJwtService jwtService;
    @Mock
    private ACIRegistrationService registrationService;
    @Mock
    private EventPublisher eventPublisher;
    private ACIJwtAuthProvider jwtAuthProvider;
    private NoAuthAuthProvider unauthenticatedProvider;
    private PullRequestRemoteRestpoint remoteRestClient;

    @Pact(provider = "bitbucket", consumer = "dvcs_connector")
    public PactFragment getPullRequestWithUnauthenticatedAccessFragment(ConsumerPactBuilder.PactDslWithProvider builder) {
        return builder
                .given("repo with pull requests")
                .uponReceiving("GET prs with unauthenticated access")
                .method("GET")
                .matchPath(PRS_API_PATTERN, PRS_API_EXAMPLE)
                .willRespondWith()
                .status(200)
                .headers(ImmutableMap.of(
                        "Content-Type", "application/json; charset=utf-8"))
                .body(new PactDslJsonBody()
                        .stringMatcher("title", "Pull request for PACT testing matters")
                        .stringMatcher("state", "OPEN")
                        .integerType("id", Integer.valueOf(PR_ID_EXAMPLE))
                        .stringMatcher("description", "This pull request was created to perform a PACT test on the pull requests rest endpoint")
                        .asBody())
                .toFragment();
    }

    @Pact(provider = "bitbucket", consumer = "dvcs_connector")
    public PactFragment getPRsWithInvalidAccountName(ConsumerPactBuilder.PactDslWithProvider builder) {
        return builder
                .given("non-existent org")
                .uponReceiving("GET pull requests with invalid account name")
                .method("GET")
                .matchPath(PRS_API_PATTERN, PRS_API_NON_EXISTENT_EXAMPLE)
                .willRespondWith()
                .status(404)
                .headers(ImmutableMap.of(
                        "Content-Type", "application/json; charset=utf-8"))
                .toFragment();
    }

    @Before
    public void setup() throws Exception {
        unauthenticatedProvider = new NoAuthAuthProvider(pactProvider.getConfig().url(), httpClientProvider);
        unauthenticatedProvider.setApiVersion(2);
    }

    @Test
    @PactVerification(value = "bitbucket", fragment = "getPullRequestWithUnauthenticatedAccessFragment")
    public void testGetPullRequestWithPublicReposAndAnonymousCredentials() {
        remoteRestClient = new PullRequestRemoteRestpoint(unauthenticatedProvider.provideRequestor());

        final BitbucketPullRequest pullRequest = remoteRestClient.getPullRequestDetail(ACCOUNT_NAME, REPO_NAME, PR_ID_EXAMPLE);

        assertThat(pullRequest).isNotNull();
        assertThat(pullRequest.getTitle()).isEqualTo("Pull request for PACT testing matters");
        assertThat(pullRequest.getState()).isEqualTo("OPEN");
        assertThat(pullRequest.getDescription()).isEqualTo("This pull request was created to perform a PACT test on the pull requests rest endpoint");
        assertThat(pullRequest.getId()).isEqualTo(Integer.valueOf(PR_ID_EXAMPLE));

    }

    @Test(expected = BitbucketRequestException.NotFound_404.class)
    @PactVerification(value = "bitbucket", fragment = "getPRsWithInvalidAccountName")
    public void testGetPullRequestsWithInvalidAccount() {
        remoteRestClient = new PullRequestRemoteRestpoint(unauthenticatedProvider.provideRequestor());

        remoteRestClient.getPullRequestDetail("non_existent", REPO_NAME, PR_ID_EXAMPLE);
    }

}
