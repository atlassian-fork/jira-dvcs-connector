package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request;

public interface ApiProvider {
    String getHostUrl();

    String getApiUrl();

    void setApiVersion(int apiVersion);

    boolean isCached();

    void setCached(boolean cached);

    boolean isCloseIdleConnections();

    void setCloseIdleConnections(boolean closeIdleConnections);
}
