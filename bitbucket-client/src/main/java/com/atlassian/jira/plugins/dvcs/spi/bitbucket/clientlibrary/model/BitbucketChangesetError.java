package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.annotation.concurrent.Immutable;
import java.io.Serializable;
import java.util.List;

/**
 * Describes Bitbucket error response
 * The error element contains an error message
 * The data element may contain a list of missing shas
 * <p>
 * <pre>
 * {
 *   "error": {
 *     "message": "Not found"
 *   },
 *   "data": {
 *     "shas": ["a1d1f2dbcfb8000c584c84a7e424acf5359eb7ae",
 *              "4e28ec75de4186e76863a9e338ce665987c03a87"]
 *     }
 * }
 * </pre>
 */
@Immutable
public class BitbucketChangesetError implements Serializable {
    private static final long serialVersionUID = 1334189507503513386L;

    private final BitbucketError error;
    private final BitbucketData data;

    @JsonCreator
    public BitbucketChangesetError(@JsonProperty("error") final BitbucketError error,
                                   @JsonProperty("data") final BitbucketData data) {
        this.error = error;
        this.data = data;
    }

    public List<String> getShas() {
        return data.getShas();
    }

    public BitbucketError getError() {
        return error;
    }

    public BitbucketData getData() {
        return data;
    }
}
