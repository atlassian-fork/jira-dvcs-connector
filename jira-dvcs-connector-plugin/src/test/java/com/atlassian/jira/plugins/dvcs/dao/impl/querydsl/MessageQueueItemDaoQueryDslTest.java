package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageQueueItemMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageTagMapping;
import com.atlassian.jira.plugins.dvcs.dao.impl.MessageQueueItemAoDaoImpl;
import com.atlassian.jira.plugins.dvcs.dao.impl.transform.MessageEntityTransformer;
import com.atlassian.jira.plugins.dvcs.model.Message;
import com.atlassian.jira.plugins.dvcs.model.MessageId;
import com.atlassian.jira.plugins.dvcs.model.MessageQueueItem;
import com.atlassian.jira.plugins.dvcs.model.MessageState;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QMessageQueueItemMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QMessageTagMapping;
import com.atlassian.jira.plugins.dvcs.service.message.MessageAddress;
import com.atlassian.jira.plugins.dvcs.service.message.MessageAddressService;
import com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessor;
import com.querydsl.core.Tuple;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.io.InputStream;
import java.time.temporal.ChronoField;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessorUtils.dropTable;
import static com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessorUtils.setUpDatabaseUsingSQLFile;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MessageQueueItemDaoQueryDslTest {

    protected static final int ID = 1;
    protected static final Date NOW = new Date();
    protected static final String QUEUE = "TestQueue";
    protected static final String STATE_INFO = "STATE INFO";
    protected static final String TAG_TWO = "tag2";
    protected static final MessageAddress address = new MockAddress();
    private static final String AO_TABLE_NAME_PREFIX = "AO_E8B6CC";
    private static StandaloneDatabaseAccessor databaseAccessor;
    private static MessageEntityTransformer transformer;

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    private MessageQueueItemDaoQueryDsl classUnderTest;
    private Message message;
    @Mock
    private MessageAddressService messageAddressService;
    @Mock
    private MessageMapping messageMapping;
    @Mock
    private MessageQueueItemAoDaoImpl messageQueueAoDao;
    @Mock
    private MessageQueueItemDaoQueryDsl messageQueueItemDaoQueryDsl;
    @Mock
    private MessageQueueItemMapping messageQueueItemMapping;
    @Mock
    private Repository repository;

    protected static String getState(int id) {
        return MessageState.values()[id % MessageState.values().length].name();
    }

    @Before
    public void setUp() throws Exception {
        final InputStream inputStream = MessageQueueItemDaoQueryDslTest.class.getResourceAsStream("/com.atlassian.jira.plugins.dvcs.dao.impl.querydsl/MessageQueueItemDaoQueryDslTest.sql");
        databaseAccessor = setUpDatabaseUsingSQLFile(AO_TABLE_NAME_PREFIX, inputStream, MessageMapping.class, MessageTagMapping.class, MessageQueueItemMapping.class);

        transformer = new MessageEntityTransformer(messageAddressService);

        classUnderTest = new MessageQueueItemDaoQueryDsl(databaseAccessor,
                messageAddressService,
                transformer,
                messageQueueAoDao);

        setupMessage();
        setupMockMessageMapping();
        setupMockMessageQueueItemMapping();
    }

    @After
    public void finalCleanUp() throws Exception {
        dropTable("AO_E8B6CC_MESSAGE_QUEUE_ITEM", databaseAccessor);
        dropTable("AO_E8B6CC_MESSAGE", databaseAccessor);
        dropTable("AO_E8B6CC_MESSAGE_TAG", databaseAccessor);
    }

    private void setupMessage() {
        message = new Message<>();
        message.setId(1);
        message.setAddress(new MockAddress());
        message.setPayload(MockPayload.class.getCanonicalName());
        message.setPayloadType(MockPayload.class);
        message.setPriority(0);
        message.setRetriesCount(0);
        message.setTags(new String[]{TAG_TWO});
    }

    private void setupMockMessageMapping() {
        when(messageMapping.getID()).thenReturn(1);
        when(messageMapping.getAddress()).thenReturn("test-id");
        when(messageMapping.getPayload()).thenReturn(MockPayload.class.getCanonicalName());
        when(messageMapping.getPayloadType()).thenReturn(MockPayload.class.getCanonicalName());
        when(messageMapping.getPriority()).thenReturn(0);
        when(messageMapping.getQueuesItems()).thenReturn(new MessageQueueItemMapping[0]);
        when(messageMapping.getTags()).thenReturn(new MessageTagMapping[]{new TestTagMapping(TAG_TWO, 1)});
    }

    private void setupMockMessageQueueItemMapping() {
        when(messageQueueItemMapping.getLastFailed()).thenReturn(NOW);
        when(messageQueueItemMapping.getMessage()).thenReturn(messageMapping);
        when(messageQueueItemMapping.getQueue()).thenReturn(QUEUE);
        when(messageQueueItemMapping.getRetriesCount()).thenReturn(0);
        when(messageQueueItemMapping.getState()).thenReturn(getState(ID));
        when(messageQueueItemMapping.getStateInfo()).thenReturn(STATE_INFO);
    }

    @Test
    public void testCreate() throws Exception {
        Map<String, Object> messageQueueItemMap = messageQueueItemToMap(1, QUEUE, MessageState.RUNNING, STATE_INFO);
        when(messageQueueAoDao.create(messageQueueItemMap)).thenReturn(messageQueueItemMapping);

        classUnderTest.create(messageQueueItemMap);

        verify(messageQueueAoDao).create(messageQueueItemMap);
    }


    @Test
    public void testSave() throws Exception {
        when(messageQueueAoDao.getByMessageId(1)).thenReturn(new MessageQueueItemMapping[]{messageQueueItemMapping});

        MessageQueueItem queueItem = classUnderTest.getByMessageId(new MessageId(1))[0];
        queueItem.setId(1);
        queueItem.setLastFailed(new Date(0L));
        queueItem.setRetryCount(1);
        queueItem.setQueue("Some other queue");
        queueItem.setStateInfo("some other state info");

        classUnderTest.save(queueItem);
        QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();

        Tuple results = databaseAccessor.runInTransaction(connection ->
                connection.select(qMessageQueueItemMapping.all())
                        .from(qMessageQueueItemMapping)
                        .orderBy(qMessageQueueItemMapping.ID.asc())
                        .fetchFirst());

        assertThat(queueItem.getLastFailed(), is(results.get(qMessageQueueItemMapping.LAST_FAILED)));
        assertThat(queueItem.getRetryCount(), is(results.get(qMessageQueueItemMapping.RETRIES_COUNT)));
        assertThat(queueItem.getQueue(), is(results.get(qMessageQueueItemMapping.QUEUE)));
        assertThat(queueItem.getStateInfo(), is(results.get(qMessageQueueItemMapping.STATE_INFO)));
    }

    @Test
    public void testDeleteByItem() throws Exception {
        MessageQueueItem queueItem = new MessageQueueItem();
        queueItem.setId(1);


        classUnderTest.delete(queueItem);

        QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();
        assertThat("None should match the deleted ID",
                databaseAccessor.runInTransaction(connection ->
                        connection.select(qMessageQueueItemMapping.all())
                                .from(qMessageQueueItemMapping).where(qMessageQueueItemMapping.ID.eq(1))
                                .fetchCount()),
                is(0l));
    }

    @Test
    public void testDeleteById() throws Exception {
        classUnderTest.delete(1);

        QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();
        assertThat("None should match the deleted ID",
                databaseAccessor.runInTransaction(connection ->
                        connection.select(qMessageQueueItemMapping.all())
                                .from(qMessageQueueItemMapping).where(qMessageQueueItemMapping.ID.eq(1))
                                .fetchCount()),
                is(0l));
    }

    @Test
    public void testGetByMessageId() throws Exception {
        when(messageQueueAoDao.getByMessageId(ID)).thenReturn(new MessageQueueItemMapping[]{messageQueueItemMapping});

        MessageQueueItem[] resultArray = classUnderTest.getByMessageId(new MessageId(ID));

        assertThat(resultArray.length, is(1));
        MessageQueueItem resultItem = resultArray[0];
        assertThat(resultItem.getId(), is(messageQueueItemMapping.getID()));
        assertThat(resultItem.getLastFailed(), is(messageQueueItemMapping.getLastFailed()));
        assertThat(resultItem.getRetryCount(), is(messageQueueItemMapping.getRetriesCount()));
        assertThat(resultItem.getQueue(), is(messageQueueItemMapping.getQueue()));
        assertThat(resultItem.getStateInfo(), is(messageQueueItemMapping.getStateInfo()));
        assertThat(resultItem.getState(), is(messageQueueItemMapping.getState()));
    }

    @Test
    public void testGetByQueueAndMessage() throws Exception {
        when(messageQueueAoDao.getByQueueAndMessage(
                messageQueueItemMapping.getQueue(),
                messageQueueItemMapping.getMessage().getID()))
                .thenReturn(messageQueueItemMapping);

        MessageQueueItem resultItem = classUnderTest.getByQueueAndMessage(
                messageQueueItemMapping.getQueue(),
                messageQueueItemMapping.getMessage().getID()).get();

        assertThat(resultItem.getId(), is(messageQueueItemMapping.getID()));
        assertThat(resultItem.getLastFailed(), is(messageQueueItemMapping.getLastFailed()));
        assertThat(resultItem.getRetryCount(), is(messageQueueItemMapping.getRetriesCount()));
        assertThat(resultItem.getQueue(), is(messageQueueItemMapping.getQueue()));
        assertThat(resultItem.getStateInfo(), is(messageQueueItemMapping.getStateInfo()));
        assertThat(resultItem.getState(), is(messageQueueItemMapping.getState()));
    }

    @Test
    public void testGetNextItemForProcessing() throws Exception {
        when(messageQueueAoDao.getNextItemForProcessing(messageQueueItemMapping.getQueue(), address.getId())).thenReturn(messageQueueItemMapping);
        Message result = classUnderTest.getNextItemForProcessing(messageQueueItemMapping.getQueue(), address.getId());

        assertThat(result.getId() == messageMapping.getID(), is(true));
        assertThat(result, is(transformer.toMessage(messageMapping)));
    }

    @Test
    public void testGetNextItemForProcessingHandlesNull() throws Exception {
        when(messageQueueAoDao.getNextItemForProcessing(messageQueueItemMapping.getQueue(), address.getId())).thenReturn(null);
        Message result = classUnderTest.getNextItemForProcessing(messageQueueItemMapping.getQueue(), address.getId());

        assertThat(result, nullValue());
    }

    @Test
    public void testGetByStateCallsConsumerCorrectNumberOfTimes() throws Exception {
        AtomicInteger counter = new AtomicInteger(0);

        classUnderTest.getByState(MessageState.SLEEPING, id -> counter.incrementAndGet());

        assertThat(counter.get(), is(2));
    }

    @Test
    public void testGetByStateCallsConsumerOnCorrectThings() throws Exception {
        final MessageState state = MessageState.SLEEPING;
        classUnderTest.getByState(state, messageId -> assertMessageQueueItemHasCorrectState(messageId, state));
    }

    private void assertMessageQueueItemHasCorrectState(Integer messageId, MessageState state) {
        QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();

        assertThat(databaseAccessor.runInTransaction(connection ->
                connection.select(qMessageQueueItemMapping.ID)
                        .from(qMessageQueueItemMapping)
                        .where(qMessageQueueItemMapping.ID.eq(messageId)
                                .and(qMessageQueueItemMapping.STATE.eq(state.name())))
                        .fetchCount()), is(1L));
    }

    @Test
    public void testGetByTagAndStateCallsConsumerCorrectNumberOfTimes() throws Exception {
        AtomicInteger counter = new AtomicInteger(0);

        classUnderTest.getByTagAndState(TAG_TWO, MessageState.SLEEPING, id -> counter.incrementAndGet());

        assertThat(counter.get(), is(1));
    }

    @Test
    public void testGetByTagAndStateCallsConsumerOnCorrectThings() throws Exception {
        final MessageState state = MessageState.SLEEPING;

        classUnderTest.getByTagAndState(TAG_TWO, MessageState.SLEEPING, messageId ->
                assertMessageQueueItemHasCorrectStateAndTag(messageId, state, TAG_TWO));
    }

    private void assertMessageQueueItemHasCorrectStateAndTag(Integer messageId, MessageState state, String tag) {
        QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();
        QMessageTagMapping qMessageTagMapping = new QMessageTagMapping();

        assertThat(databaseAccessor.runInTransaction(connection ->
                connection.select(qMessageQueueItemMapping.ID)
                        .from(qMessageQueueItemMapping).join(qMessageTagMapping).on(qMessageQueueItemMapping.MESSAGE_ID.eq(qMessageTagMapping.MESSAGE_ID))
                        .where(qMessageQueueItemMapping.ID.eq(messageId)
                                .and(qMessageQueueItemMapping.STATE.eq(state.name()))
                                .and(qMessageTagMapping.TAG.eq(tag)))
                        .fetchCount()), is(1L));
    }

    @Test
    public void getByTagAndStateHandlesEmptyTable() {
        deleteAllInQMessageQueueItemMapping();

        AtomicInteger counter = new AtomicInteger(0);
        classUnderTest.getByTagAndState(TAG_TWO, MessageState.SLEEPING, id -> counter.incrementAndGet());

        assertThat(counter.get(), is(0));
    }

    private void deleteAllInQMessageQueueItemMapping() {
        QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();

        databaseAccessor.runInTransaction(connection ->
                connection.delete(qMessageQueueItemMapping)
                        .where(qMessageQueueItemMapping.ID.eq(qMessageQueueItemMapping.ID))
                        .execute());
    }


    @Test
    public void getByStateHandlesEmptyTable() {
        deleteAllInQMessageQueueItemMapping();

        AtomicInteger counter = new AtomicInteger(0);
        classUnderTest.getByState(MessageState.SLEEPING, id -> counter.incrementAndGet());

        assertThat(counter.get(), is(0));
    }

    @Test
    public void getQueueItemByIdHappyPath() {
        Optional<MessageQueueItem> responseOption = classUnderTest.getQueueItemById(ID);

        assertThat(responseOption.isPresent(), is(true));
        MessageQueueItem result = responseOption.get();
        assertThat(result.getId(), is(1));
        assertThat(result.getState(), is(MessageState.SLEEPING.name()));
        assertThat(result.getQueue(), is(QUEUE));
        assertThat(result.getStateInfo(), is(STATE_INFO));

        assertThat(result.getLastFailed().toInstant().getLong(ChronoField.INSTANT_SECONDS), is(1009136393L));//  "2001-12-23 14:39:53.662522-05", is(result.getLastFailed()));
    }


    /**
     * Re-maps provided data to {@link MessageQueueItemMapping} parameters.
     *
     * @param messageId {@link Message#getId()}
     * @param queue     the queue
     * @param state     the message state
     * @param stateInfo ?
     * @return mapped entity
     */
    private Map<String, Object> messageQueueItemToMap(int messageId, String queue, MessageState state, String stateInfo) {
        final Map<String, Object> result = new HashMap<>();

        result.put(MessageQueueItemMapping.MESSAGE, messageId);
        result.put(MessageQueueItemMapping.QUEUE, queue);
        result.put(MessageQueueItemMapping.STATE, state.name());
        result.put(MessageQueueItemMapping.STATE_INFO, stateInfo);
        result.put(MessageQueueItemMapping.RETRIES_COUNT, 0);

        return result;
    }
}