package com.atlassian.jira.plugins.dvcs.streams;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.plugins.dvcs.model.DvcsUser;
import com.atlassian.jira.plugins.dvcs.model.GlobalFilter;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.ChangesetService;
import com.atlassian.jira.plugins.dvcs.service.RepositoryService;
import com.atlassian.jira.plugins.dvcs.webwork.IssueAndProjectKeyManager;
import com.atlassian.jira.plugins.dvcs.webwork.IssueLinker;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.ActivityVerbs;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsFeed;
import com.atlassian.streams.api.StreamsFilterType;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.streams.spi.CancelledException;
import com.atlassian.streams.spi.UserProfileAccessor;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.permission.ProjectPermissions.VIEW_DEV_TOOLS;
import static com.atlassian.jira.plugins.dvcs.streams.DvcsStreamsActivityProvider.SUMMARY_TEMPLATE_NAME;
import static com.atlassian.jira.plugins.dvcs.streams.DvcsStreamsActivityProvider.TITLE_TEMPLATE_NAME;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DvcsStreamsActivityProviderTest {
    private static final String APPLICATION_DISPLAY_NAME = "APPLICATION";
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private ChangesetService changesetService;

    @Mock
    private I18nResolver i18nResolver;

    @Mock
    private IssueAndProjectKeyManager issueAndProjectKeyManager;

    @Mock
    private IssueLinker issueLinker;

    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private ProjectManager projectManager;

    @Mock
    private RepositoryService repositoryService;

    @Mock
    private TemplateRenderer templateRenderer;

    @Mock
    private UserProfileAccessor userProfileAccessor;

    @Captor
    private ArgumentCaptor<GlobalFilter> filterCaptor;

    @InjectMocks
    private DvcsStreamsActivityProvider dvcsStreamsActivityProvider;

    // Test state to use
    private Project project1;
    private Project project2;
    private Repository repository;
    private DvcsUser author1;
    private DvcsUser author2;
    private Changeset changeset1;
    private Changeset changeset2;
    private Changeset changeset3;
    private ActivityRequest request;

    @Before
    public void setup() throws Exception {
        when(i18nResolver.getText(any(String.class))).then(i -> i.getArguments()[0]);
        doAnswer(mockRenderTemplate()).when(templateRenderer).render(any(), any(), any());
        when(applicationProperties.getDisplayName()).thenReturn(APPLICATION_DISPLAY_NAME);

        project1 = createProject("PROJECT1");
        project2 = createProject("PROJECT2");
        repository = createRepository(1);
        author1 = createDvcsUser("author1");
        author2 = createDvcsUser("author2");
        changeset1 = createChangeset(repository.getId(), "1111111111111", author1.getUsername(), "2015-01-01");
        changeset2 = createChangeset(repository.getId(), "2222222222222", author2.getUsername(), "2015-01-02");
        changeset3 = createChangeset(repository.getId(), "3333333333333", author1.getUsername(), "2015-01-03");
        request = createRequest();

        // Default test state. Override in individual tests if needed
        // (e.g. tests need only define a delta to this default state)
        setAvailableProjects(project1);
        grantViewDevToolsPermission(project1);
        setLatestChangesets(changeset1, changeset2, changeset3);
    }

    @Test
    public void getActivityFeed_shouldReturnEmptyFeed_ifNoProjectsExistInSystem() throws Exception {
        setAvailableProjects();

        final StreamsFeed streamsFeed = dvcsStreamsActivityProvider.getActivityFeed(request).call();

        assertFeedCorrect(streamsFeed);
    }

    @Test
    public void getActivityFeed_shouldReturnFeed_ifChangesetsExistAndUserHasPermission() throws Exception {
        final StreamsFeed streamsFeed = dvcsStreamsActivityProvider.getActivityFeed(request).call();

        assertFeedCorrect(streamsFeed, changeset1, changeset2, changeset3);
    }

    @Test
    public void getActivityFeed_shouldReturnEmptyFeed_ifNoChangesetsExist() throws Exception {
        setLatestChangesets();

        final StreamsFeed streamsFeed = dvcsStreamsActivityProvider.getActivityFeed(request).call();

        assertFeedCorrect(streamsFeed);
    }

    @Test
    public void getActivityFeed_shouldReturnEmptyFeed_ifUserDoesNotHavePermission() throws Exception {
        revokeViewDevToolsPermission(project1);

        final StreamsFeed streamsFeed = dvcsStreamsActivityProvider.getActivityFeed(request).call();

        assertFeedCorrect(streamsFeed);
    }

    @Test(expected = CancelledException.class)
    public void getActivityFeed_shouldThrowException_ifCalledAfterCancellation() throws Exception {
        final CancellableTask<StreamsFeed> task = dvcsStreamsActivityProvider.getActivityFeed(request);

        final CancellableTask.Result result = task.cancel();
        assertThat(result, is(CancellableTask.Result.CANCELLED));

        task.call();
    }

    @Test
    public void getActivityFeed_shouldOnlyReturnEntries_forChangesetsFromValidNonDeletedRepositories() throws Exception {
        final Changeset changesetFromDeletedRepo =
                createChangeset(100, "changesetFromDeletedRepo", author1.getUsername(), "2015-01-10");

        setLatestChangesets(changeset1, changesetFromDeletedRepo);

        final StreamsFeed streamsFeed = dvcsStreamsActivityProvider.getActivityFeed(request).call();

        assertFeedCorrect(streamsFeed, changeset1);
    }

    @Test
    public void getActivityFeed_shouldOnlyContainSingleEntryPerChangeset_whenDuplicateChangesets() throws Exception {
        setLatestChangesets(changeset1, changeset1, changeset2, changeset1, changeset3);

        final StreamsFeed streamsFeed = dvcsStreamsActivityProvider.getActivityFeed(request).call();

        assertFeedCorrect(streamsFeed, changeset1, changeset2, changeset3);
    }

    @Test
    public void getActivityFeed_filterContainsOnlyProjectsWithDevToolPermissions() throws Exception {
        setAvailableProjects(project1, project2);
        grantViewDevToolsPermission(project1);

        dvcsStreamsActivityProvider.getActivityFeed(request).call();
        verify(changesetService).getLatestChangesets(anyInt(), filterCaptor.capture());

        final GlobalFilter filter = filterCaptor.getValue();
        assertThat(filter.getInProjects(), containsInAnyOrder(project1.getKey()));
        assertThat(filter.getNotInProjects(), emptyIterable());
    }

    @Test
    public void getActivityFeed_filterIncludesProjectsFromRequest() throws Exception {
        final Project project1 = createProject("PROJECT1", "PROJECT1-OLD1", "PROJECT1-OLD2");
        final Project project2 = createProject("PROJECT2", "PROJECT2-OLD1", "PROJECT2-OLD2");

        addProjectFilter(request, StreamsFilterType.Operator.IS, project2.getKey());
        addProjectFilter(request, StreamsFilterType.Operator.NOT, project1.getKey());

        setAvailableProjects(project1, project2);
        grantViewDevToolsPermission(project1, project2);

        dvcsStreamsActivityProvider.getActivityFeed(request).call();
        verify(changesetService).getLatestChangesets(anyInt(), filterCaptor.capture());

        final GlobalFilter filter = filterCaptor.getValue();
        assertThat(filter.getInProjects(), containsInAnyOrder("PROJECT2", "PROJECT2-OLD1", "PROJECT2-OLD2"));
        assertThat(filter.getNotInProjects(), containsInAnyOrder("PROJECT1", "PROJECT1-OLD1", "PROJECT1-OLD2"));
    }

    @Test
    public void getActivityFeed_filterIncludesIssuesFromRequest() throws Exception {
        final Issue issue1 = createIssue("ISSUE1", "ISSUE1-OLD1", "ISSUE1-OLD2");
        final Issue issue2 = createIssue("ISSUE2", "ISSUE2-OLD1", "ISSUE2-OLD2");

        addIssueFilter(request, StreamsFilterType.Operator.IS, issue1.getKey());
        addIssueFilter(request, StreamsFilterType.Operator.NOT, issue2.getKey());

        dvcsStreamsActivityProvider.getActivityFeed(request).call();
        verify(changesetService).getLatestChangesets(anyInt(), filterCaptor.capture());

        final GlobalFilter filter = filterCaptor.getValue();
        assertThat(filter.getInIssues(), containsInAnyOrder("ISSUE1", "ISSUE1-OLD1", "ISSUE1-OLD2"));
        assertThat(filter.getNotInIssues(), containsInAnyOrder("ISSUE2", "ISSUE2-OLD1", "ISSUE2-OLD2"));
    }

    @Test
    public void getActivityFeed_filterIncludesUsersFromRequest() throws Exception {
        addUserFilter(request, StreamsFilterType.Operator.IS, "user1", "user2");
        addUserFilter(request, StreamsFilterType.Operator.NOT, "user3", "user4");

        dvcsStreamsActivityProvider.getActivityFeed(request).call();
        verify(changesetService).getLatestChangesets(anyInt(), filterCaptor.capture());

        final GlobalFilter filter = filterCaptor.getValue();
        assertThat(filter.getInUsers(), containsInAnyOrder("user1", "user2"));
        assertThat(filter.getNotInUsers(), containsInAnyOrder("user3", "user4"));
    }

    /* ----------------------------------------------
     * Setup and verification code - Here be dragons!
     * ----------------------------------------------
     */

    private void assertFeedCorrect(final StreamsFeed feed, final Changeset... changesets) {
        assertThat(feed, notNullValue());
        assertThat(feed.getTitle(), is(DvcsStreamsActivityProvider.STREAMS_EXTERNAL_FEED_TITLE_KEY));

        final ArrayList<StreamsEntry> entries = newArrayList(feed.getEntries());
        assertThat(entries.size(), is(changesets.length));

        for (int i = 0; i < entries.size(); i++) {
            final StreamsEntry entry = entries.get(i);
            final Changeset changeset = changesets[i];
            assertEntryCorrect(entry, changeset);
        }
    }

    private void assertEntryCorrect(final StreamsEntry entry, final Changeset changeset) {
        assertThat(entry, notNullValue());
        assertThat(entry.getPostedDate().toDate(), is(changeset.getDate()));
        assertThat(entry.getVerb(), is(ActivityVerbs.update()));
        assertThat(entry.getApplicationType(), is(APPLICATION_DISPLAY_NAME));

        final StreamsEntry.ActivityObject activityObject = entry.getActivityObjects().iterator().next();
        assertThat(activityObject.getTitle().isDefined(), is(false));
        assertThat(activityObject.getSummary().isDefined(), is(false));
        assertThat(activityObject.getId().get(), is(changeset.getNode()));

        // This is a bit hacky, but we know that the string representation from the mock renderer
        // contains some info useful for making assertions on.
        final String content = entry.renderContentAsHtml().get().toString();
        assertThat(content, containsString(SUMMARY_TEMPLATE_NAME));
        assertThat(content, containsString(changeset.toString()));

        final String title = entry.renderTitleAsHtml().toString();
        assertThat(title, containsString(TITLE_TEMPLATE_NAME));
        assertThat(title, containsString(changeset.toString()));
    }

    private void setLatestChangesets(final Changeset... changesets) {
        when(changesetService.getLatestChangesets(anyInt(), any())).thenReturn(asList(changesets));
        when(changesetService.getChangesetsWithFileDetails(any())).thenReturn(asList(changesets));
    }

    private void setAvailableProjects(final Project... projects) {
        when(projectManager.getProjectObjects()).thenReturn(asList(projects));
        for (Project p : projects) {
            when(projectManager.getProjectByCurrentKey(p.getKey())).thenReturn(p);
            when(projectManager.getProjectObjByKey(p.getKey())).thenReturn(p);
        }
    }

    private Project createProject(final String key, final String... historicKeys) {
        final Project project = mock(Project.class);
        when(project.getKey()).thenReturn(key);

        final Set<String> allKeys = ImmutableSet.<String>builder().add(key).add(historicKeys).build();
        when(issueAndProjectKeyManager.getAllProjectKeys(project)).thenReturn(allKeys);
        when(issueAndProjectKeyManager.getAllProjectKeys(key)).thenReturn(allKeys);

        return project;
    }

    private Issue createIssue(final String key, final String... historicKeys) {
        final Issue issue = mock(Issue.class);
        when(issue.getKey()).thenReturn(key);

        final Set<String> allKeys = ImmutableSet.<String>builder().add(key).add(historicKeys).build();
        when(issueAndProjectKeyManager.getAllIssueKeys(issue)).thenReturn(allKeys);
        when(issueAndProjectKeyManager.getAllIssueKeys(key)).thenReturn(allKeys);

        return issue;
    }

    private Changeset createChangeset(final int repoId, final String node,
                                      final String author, final String creationDate) throws Exception {
        final Changeset changeset = new Changeset(repoId, node, "A dummy message", dateFormat.parse(creationDate));
        changeset.setAuthor(author);
        changeset.setRawAuthor(author);
        return changeset;
    }

    private DvcsUser createDvcsUser(final String username) {
        final DvcsUser dvcsUser = new DvcsUser(username, username, username, "https://avatar/" + username, "https://remote/" + username);
        when(repositoryService.getUser(any(), eq(username), eq(username))).thenReturn(dvcsUser);
        return dvcsUser;
    }

    private Repository createRepository(final int id) {
        final Repository repo = mock(Repository.class);
        when(repo.getId()).thenReturn(id);
        when(repositoryService.get(id)).thenReturn(repo);
        return repo;
    }

    private ActivityRequest createRequest() {
        final ActivityRequest request = mock(ActivityRequest.class);
        when(request.getStandardFilters()).thenReturn(ArrayListMultimap.create());
        when(request.getMaxResults()).thenReturn(1000);
        return request;
    }

    private void addProjectFilter(final ActivityRequest request,
                                  final StreamsFilterType.Operator op, final String... projectKeys) {
        request.getStandardFilters().put(PROJECT_KEY, pair(op, asList(projectKeys)));
    }

    private void addIssueFilter(final ActivityRequest request,
                                final StreamsFilterType.Operator op, final String... issueKeys) {
        request.getStandardFilters().put(ISSUE_KEY.getKey(), pair(op, asList(issueKeys)));
    }

    private void addUserFilter(final ActivityRequest request,
                               final StreamsFilterType.Operator op, final String... usernames) {
        request.getStandardFilters().put(USER.getKey(), pair(op, asList(usernames)));
    }

    private void grantViewDevToolsPermission(final Project... projects) {
        for (Project project : projects) {
            when(permissionManager.hasPermission(eq(VIEW_DEV_TOOLS), eq(project), any())).thenReturn(true);
        }
    }

    private void revokeViewDevToolsPermission(final Project... projects) {
        for (Project project : projects) {
            when(permissionManager.hasPermission(eq(VIEW_DEV_TOOLS), eq(project), any())).thenReturn(false);
        }
    }

    private Answer mockRenderTemplate() {
        return i -> {
            final String template = (String) i.getArguments()[0];
            final Map<String, Object> context = (Map<String, Object>) i.getArguments()[1];
            final StringWriter sw = (StringWriter) i.getArguments()[2];
            sw.append(template);
            sw.append(System.lineSeparator());
            context.forEach((k, v) -> sw
                    .append('\t')
                    .append(k).append(":").append(v == null ? "null" : v.toString())
                    .append(System.lineSeparator()));
            return null;
        };
    }
}