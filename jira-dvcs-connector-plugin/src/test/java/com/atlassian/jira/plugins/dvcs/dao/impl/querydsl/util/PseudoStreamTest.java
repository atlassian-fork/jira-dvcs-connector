package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl.util;

import com.google.common.collect.ImmutableList;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Expression;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class PseudoStreamTest {

    private static final String MODEL_STRING = "dummy text";

    @Mock
    private Supplier<Integer> startingIndexSupplier;

    @Mock
    private Function<Integer, List<Tuple>> getNextPageOfResults;

    @Mock
    private Function<List<Tuple>, Optional<Integer>> getPossiblyBeMaxIndexInResultsPage;

    @Mock
    private Function<Tuple, String> convertRawResultsToModel;

    @Mock
    private Consumer<String> modelConsumer;

    private List<Tuple> page0 = ImmutableList.of(new TestTuple(), new TestTuple(), new TestTuple());
    private List<Tuple> page1 = ImmutableList.of(new TestTuple(), new TestTuple(), new TestTuple());
    private List<Tuple> page2 = ImmutableList.of(new TestTuple(), new TestTuple(), new TestTuple());

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);

        when(startingIndexSupplier.get()).thenReturn(0);

        when(getNextPageOfResults.apply(0)).thenReturn(page0);
        when(getNextPageOfResults.apply(1)).thenReturn(page1);
        when(getNextPageOfResults.apply(2)).thenReturn(page2);

        when(convertRawResultsToModel.apply(any(TestTuple.class))).thenReturn(MODEL_STRING);

        when(getPossiblyBeMaxIndexInResultsPage.apply(page0)).thenReturn(Optional.of(1));
        when(getPossiblyBeMaxIndexInResultsPage.apply(page1)).thenReturn(Optional.of(2));
        when(getPossiblyBeMaxIndexInResultsPage.apply(page2)).thenReturn(Optional.empty());
    }

    @Test
    public void testConsumeAllInTable() throws Exception {
        PseudoStream.consumeAllInTable(startingIndexSupplier,
                getNextPageOfResults,
                getPossiblyBeMaxIndexInResultsPage,
                convertRawResultsToModel,
                modelConsumer);

        verify(convertRawResultsToModel, times(9)).apply(any(TestTuple.class));
        verify(modelConsumer, times(9)).accept(MODEL_STRING);
    }

    private class TestTuple implements Tuple {
        @Nullable
        @Override
        public <T> T get(int index, Class<T> type) {
            return null;
        }

        @Nullable
        @Override
        public <T> T get(Expression<T> expr) {
            return null;
        }

        @Override
        public int size() {
            return 0;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }
    }
}