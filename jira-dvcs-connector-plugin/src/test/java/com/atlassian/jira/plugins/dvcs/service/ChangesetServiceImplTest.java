package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.ChangesetMapping;
import com.atlassian.jira.plugins.dvcs.dao.ChangesetDao;
import com.atlassian.jira.plugins.dvcs.dao.RepositoryDao;
import com.atlassian.jira.plugins.dvcs.event.ChangesetCreatedEvent;
import com.atlassian.jira.plugins.dvcs.event.DevSummaryChangedEvent;
import com.atlassian.jira.plugins.dvcs.event.ThreadEvents;
import com.atlassian.jira.plugins.dvcs.exception.SourceControlException;
import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFile;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFileAction;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFileDetail;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFileDetailsEnvelope;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicator;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicatorProvider;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.BitbucketCommunicator.BITBUCKET;
import static com.google.common.collect.ImmutableList.of;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class ChangesetServiceImplTest {
    private static final int REPOSITORY_ID = 33;

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ThreadEvents threadEvents;

    @Mock
    private ChangesetDao changesetDao;

    @Mock
    private ClusterLockService clusterLockService;

    @Mock
    private ClusterLock clusterLock;

    @Mock
    private RepositoryDao repositoryDao;

    @Mock
    private DvcsCommunicator dvcsCommunicator;

    @Mock
    private DvcsCommunicatorProvider dvcsCommunicatorProvider;

    private Repository repository;

    private ImmutableList.Builder<Changeset> builder;

    private Changeset changesetWithData;
    private Changeset changesetWithoutData;
    private ChangesetFileDetail fileDetail;

    private ChangesetServiceImpl changesetService;

    @Before
    public void setup() {
        when(clusterLockService.getLockForName(anyString())).thenReturn(clusterLock);

        changesetService = new ChangesetServiceImpl(changesetDao, clusterLockService);
        ReflectionTestUtils.setField(changesetService, "repositoryDao", repositoryDao);
        ReflectionTestUtils.setField(changesetService, "threadEvents", threadEvents);
        ReflectionTestUtils.setField(changesetService, "dvcsCommunicatorProvider", dvcsCommunicatorProvider);

        repository = new Repository();
        repository.setId(REPOSITORY_ID);
        repository.setDvcsType(BITBUCKET);
        when(repositoryDao.get(anyInt())).thenReturn(repository);

        builder = ImmutableList.builder();

        changesetWithData = new Changeset(REPOSITORY_ID, "123-sdkj", "something", new Date());
        changesetWithData.setAllFileCount(1);
        changesetWithData.setFileDetails(new LinkedList<>());

        changesetWithoutData = new Changeset(REPOSITORY_ID, "9834lk-gffg", "nothing", new Date());

        fileDetail = new ChangesetFileDetail(ChangesetFileAction.ADDED, "foo.txt", 1, 0);

        when(dvcsCommunicatorProvider.getCommunicator(BITBUCKET)).thenReturn(dvcsCommunicator);
    }

    @Test
    public void createShouldBroadcastChangesetCreatedEventForNewChangesets() throws Exception {
        final String issueKey = "DEV-1";
        final Changeset changeset = createChangeset(issueKey, true);

        changesetService.create(changeset, ImmutableSet.of(issueKey));

        final ArgumentCaptor<Object> eventCaptor = ArgumentCaptor.forClass(Object.class);
        verify(threadEvents, times(2)).broadcast(eventCaptor.capture());
        verify(repositoryDao).get(anyInt());

        assertThat(eventCaptor.getAllValues().get(0), instanceOf(ChangesetCreatedEvent.class));
        final ChangesetCreatedEvent event = (ChangesetCreatedEvent) eventCaptor.getAllValues().get(0);
        assertThat(event.getChangeset(), is(changeset));

        assertThat(eventCaptor.getAllValues().get(1), instanceOf(DevSummaryChangedEvent.class));
        assertThat(event.getIssueKeys(), contains(issueKey));
    }

    @Test
    public void createShouldNotBroadcastChangesetCreatedEventForExistingChangesets() throws Exception {
        final String issueKey = "DEV-1";
        final Changeset changeset = createChangeset(issueKey, false);

        changesetService.create(changeset, ImmutableSet.of(issueKey));

        verify(threadEvents, never()).broadcast(anyObject());
    }

    @Test
    public void testHappyPathWithExistingData() throws Exception {
        final List<Changeset> result = changesetService.getChangesetsWithFileDetails(of(changesetWithData));

        assertThat(result, containsInAnyOrder(changesetWithData));
    }

    @Test
    public void testChangesetWithFileDetailsIsNotMigrated() throws Exception {
        changesetService.processRepository(repository, of(changesetWithData), dvcsCommunicator, builder);

        final ImmutableList<Changeset> processedChangesets = builder.build();
        assertThat(processedChangesets, containsInAnyOrder(changesetWithData));
    }

    @Test
    public void testChangesetWithoutFileDetailsIsMigratedFromDB() throws Exception {
        setFileDetails(changesetWithoutData, fileDetail);

        changesetService.processRepository(repository, of(changesetWithoutData), dvcsCommunicator, builder);

        final List<Changeset> processedChangesets = builder.build();
        assertThat(processedChangesets.size(), equalTo(1));
        assertThat(processedChangesets.get(0).getFileDetails(), containsInAnyOrder(fileDetail));
    }

    @Test
    public void testChangesetWithoutFileDetailsIsMigratedFromBB() throws Exception {
        when(changesetDao.migrateFilesData(changesetWithoutData, BITBUCKET)).thenReturn(changesetWithoutData);
        final ChangesetFileDetailsEnvelope changesetFileDetailsEnvelope = new ChangesetFileDetailsEnvelope(of(fileDetail), 1);
        when(dvcsCommunicator.getFileDetails(repository, changesetWithoutData)).thenReturn(changesetFileDetailsEnvelope);
        when(changesetDao.update(changesetWithoutData)).thenReturn(changesetWithoutData);

        changesetService.processRepository(repository, of(changesetWithoutData), dvcsCommunicator, builder);

        final List<Changeset> processedChangesets = builder.build();
        assertThat(processedChangesets.size(), equalTo(1));
        final Changeset processedChangeset = processedChangesets.get(0);
        assertThat(processedChangeset.getFileDetails(), containsInAnyOrder(fileDetail));
        assertThat(processedChangeset.getAllFileCount(), equalTo(1));
    }

    @Test
    public void testChangesetWithoutFileDetailsStillInResultWhenExceptionThrown() throws Exception {
        when(changesetDao.migrateFilesData(changesetWithoutData, BITBUCKET)).thenReturn(changesetWithoutData);
        when(dvcsCommunicator.getFileDetails(repository, changesetWithoutData)).thenThrow(new SourceControlException());

        changesetService.processRepository(repository, of(changesetWithoutData), dvcsCommunicator, builder);

        final List<Changeset> processedChangesets = builder.build();
        assertThat(processedChangesets, containsInAnyOrder(changesetWithoutData));
    }

    @Test
    public void getFileCommitUrls_returnsEmptyMap_whenNoChangesetFiles() throws Exception {
        final Map<ChangesetFile, String> urls = changesetService.getFileCommitUrls(repository, createChangesetWithFiles());
        assertThat(urls.isEmpty(), is(true));
    }

    @Test(expected = NullPointerException.class)
    public void getFileCommitUrls_fails_whenNullChangeset() throws Exception {
        changesetService.getFileCommitUrls(repository, null);
    }

    @Test
    public void getFileCommitUrls_returnsMap_whenChangesetHasFiles() throws Exception {
        final String[] filenames = new String[]{"file1", "file2", "file3"};

        // Reflect the filename back for verification below
        when(dvcsCommunicator.getFileCommitUrl(any(), any(), any(), anyInt())).then(i -> i.getArguments()[2]);

        final Map<ChangesetFile, String> urls = changesetService.getFileCommitUrls(repository, createChangesetWithFiles(filenames));

        assertThat(urls.isEmpty(), is(false));
        assertThat(urls.values(), containsInAnyOrder(filenames));
    }

    @Test
    public void checkChangesetVersion_doesNothing_whenNullChangeset() throws Exception {
        changesetService.checkChangesetVersion(null);

        verify(dvcsCommunicator, never()).getChangeset(any(), any());
        verify(changesetDao, never()).update(any());
    }

    @Test
    public void checkChangesetVersion_doesNothing_whenChangesetVersionIsCurrent() throws Exception {
        final Changeset changeset = createChangesetWithDataVersion(ChangesetMapping.LATEST_VERSION);

        changesetService.checkChangesetVersion(changeset);

        verify(dvcsCommunicator, never()).getChangeset(any(), any());
        verify(changesetDao, never()).update(any());
    }

    @Test
    public void checkChangesetVersion_updatesStoredData_whenChangesetVersionIsOld() throws Exception {
        final Changeset changeset = createChangesetWithDataVersion(ChangesetMapping.LATEST_VERSION - 1);
        when(dvcsCommunicator.getChangeset(any(), any())).thenReturn(changeset);

        changesetService.checkChangesetVersion(changeset);

        verify(dvcsCommunicator, times(1)).getChangeset(any(), any());
        verify(changesetDao, times(1)).update(any());
    }

    private void setFileDetails(final Changeset changeset, final ChangesetFileDetail fileDetail) {
        when(changesetDao.migrateFilesData(changeset, BITBUCKET)).thenAnswer(invocation -> {
            final Changeset suppliedChangeset = (Changeset) invocation.getArguments()[0];
            final List<ChangesetFileDetail> detailsList = new ArrayList<>();
            detailsList.add(fileDetail);
            suppliedChangeset.setFileDetails(detailsList);

            return suppliedChangeset;
        });
    }

    private Changeset createChangeset(final String issueKey, final boolean isNew) {
        final Changeset changeset = new Changeset(1, "d11320d1e9321f4ea96f9b46ecae20027a85dc7b", issueKey + ": file changed", new Date());
        when(changesetDao.createOrAssociate(changeset, ImmutableSet.of(issueKey))).thenReturn(isNew);
        return changeset;
    }

    private Changeset createChangesetWithFiles(final String... files) {
        final Changeset changeset = new Changeset(1, RandomStringUtils.random(12), "A commit", new Date());
        changeset.setFiles(asList(files)
                .stream()
                .map(f -> new ChangesetFile(ChangesetFileAction.ADDED, f))
                .collect(Collectors.toList()));
        changeset.setAllFileCount(files.length);
        return changeset;
    }

    private Changeset createChangesetWithDataVersion(int version) {
        final Changeset changeset = new Changeset(1, RandomStringUtils.random(12), "A commit", new Date());
        changeset.setVersion(version);
        return changeset;
    }
}
