package com.atlassian.jira.plugins.dvcs.activeobjects;

import net.java.ao.EntityManager;
import net.java.ao.RawEntity;

import javax.annotation.Nonnull;
import java.sql.SQLException;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class AOPopulator {

    protected final EntityManager entityManager;

    protected AOPopulator(@Nonnull final EntityManager entityManager) {
        this.entityManager = checkNotNull(entityManager);
    }

    public <T extends RawEntity<K>, K> T create(final Class<T> type, final Map<String, Object> params) {
        try {
            return entityManager.create(type, params);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
