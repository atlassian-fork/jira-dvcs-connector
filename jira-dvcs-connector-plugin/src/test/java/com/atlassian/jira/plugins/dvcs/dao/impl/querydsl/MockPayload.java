package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.model.Progress;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.message.HasProgress;


public class MockPayload implements HasProgress {

    @Override
    public Progress getProgress() {
        return null;
    }

    @Override
    public int getSyncAuditId() {
        return 0;
    }

    @Override
    public boolean isSoftSync() {
        return false;
    }

    @Override
    public boolean isWebHookSync() {
        return false;
    }

    @Override
    public Repository getRepository() {
        return null;
    }
}