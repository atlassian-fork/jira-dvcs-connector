package com.atlassian.jira.plugins.dvcs.base;

/**
 * @author Stanislav Dvorscak
 * @see #register(TestListener)
 */
public interface TestListenerDelegate {

    /**
     * Registers provided {@link TestListener}.
     *
     * @param listener for registration
     */
    void register(TestListener listener);

}
