package com.atlassian.jira.plugins.dvcs.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.SyncAuditLogMapping;
import com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService;
import com.atlassian.sal.api.transaction.TransactionCallback;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Pure unit test of this DAO.
 */
@RunWith(MockitoJUnitRunner.class)
public class SyncAuditLogDaoImplTest {

    @Mock
    private ActiveObjects activeObjects;

    @Mock
    private AnalyticsService analyticsService;

    @Captor
    private ArgumentCaptor<TransactionCallback<Void>> callbackCaptor;

    @InjectMocks
    private SyncAuditLogDaoImpl dao;

    @Test
    @SuppressWarnings("unchecked")
    public void setExceptionMethodShouldHandleInvalidSyncId() {
        // Set up
        final boolean overwriteOld = true;
        final int invalidSyncId = 666;
        final Throwable throwable = new Throwable("Fake throwable");
        when(activeObjects.get(SyncAuditLogMapping.class, invalidSyncId)).thenReturn(null);

        // Invoke
        dao.setException(invalidSyncId, throwable, overwriteOld);

        // Check
        verify(activeObjects).executeInTransaction(callbackCaptor.capture());
        final TransactionCallback<Void> callback = callbackCaptor.getValue();
        callback.doInTransaction();
    }
}
