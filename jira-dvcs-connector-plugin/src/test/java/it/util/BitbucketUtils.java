package it.util;

import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.BitbucketDetails;

import static java.lang.String.format;

/**
 * Provides test oriented utility methods related to Bitbucket.
 */
public class BitbucketUtils {
    /**
     * Creates url that can be used as a GIT remote for pushing to BB repo.
     *
     * @param accountName Bitbucket Account name
     * @param repoSlug    Repo slug of the target Bitbucket repo
     * @return url that can be used in GIT remote definition,
     * i.e.: <code>https://accountName@bitbucket.org/accountName/repoSlug.git</code>
     */
    public static String createBitbucketRepoRemoteUrl(final String accountName, final String repoSlug) {
        return BitbucketDetails.getHostUrl().replace("://", format("://%s@", accountName))
                + format("/%s/%s.git", accountName, repoSlug);
    }

}
