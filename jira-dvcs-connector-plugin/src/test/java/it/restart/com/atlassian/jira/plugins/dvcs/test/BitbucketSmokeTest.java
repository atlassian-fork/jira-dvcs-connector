package it.restart.com.atlassian.jira.plugins.dvcs.test;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.Account;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.AccountRepository;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.DvcsAccountsPage;
import com.atlassian.pageobjects.TestedProductFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Optional;

import static it.util.TestAccounts.JIRA_BB_CONNECTOR_ACCOUNT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.notNullValue;

public class BitbucketSmokeTest {

    private static final String REPOSITORY = "public-hg-repo";
    private static final String EXPECTED_SYNC_MESSAGE = "Fri Mar 02 2012";

    private static final JiraTestedProduct JIRA = TestedProductFactory.create(JiraTestedProduct.class);
    private static final DVCSTestHelper helper = new DVCSTestHelper(JIRA.environmentData());

    @BeforeClass
    public static void setup() {
        JIRA.quickLoginAsAdmin();
        helper.clearAllOrganizations();
    }

    @AfterClass
    public static void cleanup() {
        helper.clearAllOrganizations();
    }

    @Test
    public void addOrganizationWaitForSync() {
        final Organization organization = helper.addApprovedBitbucketAccount();
        final DvcsAccountsPage accountsPage = JIRA.visit(DvcsAccountsPage.class);

        final Optional<Account> mayBeAccount = accountsPage.getAccounts()
                .stream()
                .filter(account -> account.getId() == organization.getId())
                .findFirst();

        assertThat("Account " + JIRA_BB_CONNECTOR_ACCOUNT + " should exist", mayBeAccount.isPresent(), is(true));

        final Account account = mayBeAccount.get();

        final AccountRepository repository = account.enableRepository(REPOSITORY);
        assertThat("Repository " + REPOSITORY + " should exist", repository, notNullValue());

        repository.synchronize();
        assertThat(repository.getMessage().byDefaultTimeout(), equalToIgnoringCase(EXPECTED_SYNC_MESSAGE));

        helper.assertCommits("QA-2", 1, "BB modified 1 file to QA-2 and QA-3 from TestRepo-QA");
        helper.assertCommits("QA-3", 2, "BB modified 1 file to QA-2 and QA-3 from TestRepo-QA");
    }
}
