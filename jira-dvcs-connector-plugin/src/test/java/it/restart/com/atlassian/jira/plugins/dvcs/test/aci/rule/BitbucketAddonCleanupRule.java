package it.restart.com.atlassian.jira.plugins.dvcs.test.aci.rule;

import com.atlassian.jira.plugins.dvcs.pageobjects.common.BitbucketTestedProduct;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.BitbucketManageAddonsPage;
import com.atlassian.pageobjects.elements.PageElement;
import it.util.TestRule;

import javax.annotation.Nonnull;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.google.common.base.Preconditions.checkNotNull;

public class BitbucketAddonCleanupRule extends TestRule {
    private final BitbucketTestedProduct bitbucket;
    private final String bitbucketAccount;

    public BitbucketAddonCleanupRule(
            @Nonnull final BitbucketTestedProduct bitbucket,
            @Nonnull final String bitbucketAccount) {
        this.bitbucket = checkNotNull(bitbucket);
        this.bitbucketAccount = checkNotNull(bitbucketAccount);
    }

    @Override
    protected void doApply() throws Exception {
        final BitbucketManageAddonsPage manageAddonsPage = bitbucket.visit(BitbucketManageAddonsPage.class, bitbucketAccount);

        manageAddonsPage.retrieveAndExpandAllDVCSTwixies();
        final List<PageElement> removeButtons = manageAddonsPage.getAllDvcsConnectorUnInstallButtons();
        for (PageElement removeButton : removeButtons) {
            waitUntilEnabled(removeButton);
            removeButton.click();
            final PageElement visibleRemoveButtonInDialog = manageAddonsPage.getVisibleRemoveButtonInDialog();
            waitUntilEnabled(visibleRemoveButtonInDialog);
            visibleRemoveButtonInDialog.click();
        }
    }

    private void waitUntilEnabled(PageElement pageElement) {
        waitUntilTrue(pageElement.timed().isEnabled());
    }
}
