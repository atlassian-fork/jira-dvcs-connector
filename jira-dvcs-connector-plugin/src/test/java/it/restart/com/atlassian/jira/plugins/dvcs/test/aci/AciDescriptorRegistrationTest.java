package it.restart.com.atlassian.jira.plugins.dvcs.test.aci;

import com.atlassian.fusion.aci.test.ACITestClient;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.testng.annotations.Test;

import java.util.Optional;

import static com.atlassian.fusion.aci.api.service.ACIRegistrationService.DESCRIPTOR_KEY;
import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.startsWith;

public class AciDescriptorRegistrationTest extends BaseAciDependentTest {
    @Test
    public void testDescriptorExists() throws Exception {
        enableDarkFeature();
        try {
            final String baseUrl = getJiraBaseUrl();

            final ACITestClient testClient = new ACITestClient(baseUrl, ImmutableMap.of());
            final JsonParser jsonParser = new JsonParser();

            final Optional<String> descriptorOption = testClient.getDescriptor(BITBUCKET_CONNECTOR_APPLICATION_ID);

            final JsonObject rootObject = jsonParser.parse(descriptorOption.get()).getAsJsonObject();

            assertThat(rootObject.getAsJsonPrimitive(DESCRIPTOR_KEY).getAsString(), startsWith(BITBUCKET_CONNECTOR_APPLICATION_ID));
        } finally {
            resetDarkFeature();
        }
    }
}
