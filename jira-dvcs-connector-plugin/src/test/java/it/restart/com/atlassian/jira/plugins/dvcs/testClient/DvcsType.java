package it.restart.com.atlassian.jira.plugins.dvcs.testClient;

import javax.annotation.Nonnull;

public enum DvcsType {
    GIT {
        @Override
        @Nonnull
        public Dvcs newInstance() {
            return new GitDvcs();
        }
    },

    @SuppressWarnings("unused")
    MERCURIAL {
        @Override
        @Nonnull
        public Dvcs newInstance() {
            return new MercurialDvcs();
        }
    };

    /**
     * Returns a new Dvcs of this type.
     *
     * @return a non-null instance
     */
    @Nonnull
    public abstract Dvcs newInstance();
}
