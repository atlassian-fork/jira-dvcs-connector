package it.restart.com.atlassian.jira.plugins.dvcs.test.aci.rule;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import it.util.TestPrecondition;

import javax.annotation.Nonnull;

import static com.atlassian.plugin.PluginState.ENABLED;
import static com.google.common.base.Preconditions.checkNotNull;
import static it.com.atlassian.jira.plugins.PluginUtil.DVCS_PLUGIN_KEY;
import static it.com.atlassian.jira.plugins.PluginUtil.assertPluginState;

/**
 * A precondition for checking the state of the plugins required for ACI related tests
 */
public class CheckPluginsEnabledPrecondition extends TestPrecondition {
    public static final String ACI_PLUGIN_KEY = "com.atlassian.fusion.plugins.atlassian-connect-integration-plugin";
    public static final String ACI_TESTKIT_KEY = "atlassian-connect-integration-testkit";

    private static final String[] REQUIRED_PLUGINS = {
            DVCS_PLUGIN_KEY,
            ACI_PLUGIN_KEY

    };

    private static final String[] REQUIRED_TESTKITS = {
            ACI_TESTKIT_KEY
    };

    private final JiraTestedProduct jira;
    private final boolean reqireTestkit;

    public CheckPluginsEnabledPrecondition(@Nonnull final JiraTestedProduct jira, final boolean requireTestkit) {
        this.jira = checkNotNull(jira);
        this.reqireTestkit = requireTestkit;
    }

    @Override
    protected void doApply() throws Exception {
        for (String plugin : REQUIRED_PLUGINS) {
            assertPluginState(jira.backdoor(), plugin, ENABLED);
        }
        if (reqireTestkit) {
            for (String plugin : REQUIRED_TESTKITS) {
                assertPluginState(jira.backdoor(), plugin, ENABLED);
            }
        }
    }
}
