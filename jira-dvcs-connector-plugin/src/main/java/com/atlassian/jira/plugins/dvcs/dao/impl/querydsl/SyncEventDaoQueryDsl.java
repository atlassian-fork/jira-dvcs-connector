package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.dao.impl.querydsl.util.PseudoStream;
import com.atlassian.jira.plugins.dvcs.event.ContextAwareSyncEvent;
import com.atlassian.jira.plugins.dvcs.event.DefaultContextAwareSyncEvent;
import com.atlassian.jira.plugins.dvcs.event.SyncEvent;
import com.atlassian.jira.plugins.dvcs.event.SyncEventDao;
import com.atlassian.jira.plugins.dvcs.event.SyncEventMapping;
import com.atlassian.jira.plugins.dvcs.exception.InvalidSyncEventInDatabase;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QSyncEventMapping;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.google.common.collect.ImmutableMap;
import com.querydsl.core.Tuple;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import static com.atlassian.jira.plugins.dvcs.event.SyncEventMapping.EVENT_CLASS;
import static com.atlassian.jira.plugins.dvcs.event.SyncEventMapping.EVENT_DATE;
import static com.atlassian.jira.plugins.dvcs.event.SyncEventMapping.EVENT_JSON;
import static com.atlassian.jira.plugins.dvcs.event.SyncEventMapping.REPO_ID;
import static com.atlassian.jira.plugins.dvcs.event.SyncEventMapping.SCHEDULED_SYNC;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Optional.ofNullable;
import static org.slf4j.LoggerFactory.getLogger;

@Named
@ParametersAreNonnullByDefault
public class SyncEventDaoQueryDsl implements SyncEventDao {
    private static final int PAGE_SIZE = 1000;
    private static final Logger log = getLogger(SyncEventDaoQueryDsl.class);
    private final ActiveObjects ao;
    private final DatabaseAccessor databaseAccessor;

    @Inject
    public SyncEventDaoQueryDsl(@ComponentImport final ActiveObjects ao,
                                final DatabaseAccessor databaseAccessor) {
        this.ao = checkNotNull(ao);
        this.databaseAccessor = checkNotNull(databaseAccessor);
    }

    @Override
    public void save(final ContextAwareSyncEvent event) {
        try {
            String json = event.asJson();
            ao.executeInTransaction(() -> ao.create(SyncEventMapping.class, ImmutableMap.<String, Object>of(
                    REPO_ID, event.getRepoId(),
                    EVENT_DATE, event.getDate(),
                    EVENT_CLASS, event.getEventName(),
                    EVENT_JSON, json,
                    SCHEDULED_SYNC, event.scheduledSync()
            )));
        } catch (IOException e) {
            throw new IllegalArgumentException("Event must be able to serialized to JSON", e);
        }
    }

    @Override
    public void delete(final ContextAwareSyncEvent event) {
        final int id = event.getId().orElseThrow(() -> new IllegalArgumentException("event argument must have an Id"));
        databaseAccessor.runInTransaction(databaseConnection ->
        {
            final QSyncEventMapping qSyncEventMapping = new QSyncEventMapping();
            return databaseConnection.delete(qSyncEventMapping)
                    .where(qSyncEventMapping.ID.eq(id))
                    .execute();
        });
    }


    @Override
    public long deleteAll(int repoId) {
        return databaseAccessor.runInTransaction(connection -> {
            final QSyncEventMapping qSyncEventMapping = new QSyncEventMapping();
            return connection.delete(qSyncEventMapping)
                    .where(qSyncEventMapping.REPO_ID.eq(repoId))
                    .execute();
        });
    }

    @Override
    public void foreachByRepoId(int repoId, final Consumer<ContextAwareSyncEvent> consumer) {
        PseudoStream.consumeAllInTable(
                () -> getStartingID(repoId),
                (id) -> getPageOfResults(repoId, id),
                this::getMaxIdIfPresent,
                this::toContextAwareEvent,
                consumer
        );
    }

    /**
     * Returns the largest ID that is strictly less than the minimum ID found in the result set
     * ie one less that the minimum ID number in use
     *
     * @param repoId the repoId we are selecting on
     * @return the largest Id that is smaller than minimum ID value found
     */
    private int getStartingID(final int repoId) {
        List<Integer> returnedValue = databaseAccessor.runInTransaction(connection -> {
            final QSyncEventMapping qSyncEventMapping = new QSyncEventMapping();
            return connection.select(qSyncEventMapping.ID.min()).from(qSyncEventMapping)
                    .where(qSyncEventMapping.REPO_ID.eq(repoId))
                    .fetch();
        });

        return returnedValue.stream()
                .map(Optional::ofNullable)
                .findFirst()
                .get().orElse(0) - 1;
    }

    /**
     * Gets the next page of results from the DB that have an ID greater than the argumentId and also match the given repoId,
     * when the table is in ascending order by ID.
     *
     * @param repoId              the repoId we are matching on
     * @param maxIDinPreviousPage the ID number all results have be greater than
     * @return a page of results
     */
    private List<Tuple> getPageOfResults(final int repoId, final int maxIDinPreviousPage) {
        return databaseAccessor.runInTransaction(connection -> {
            final QSyncEventMapping qSyncEventMapping = new QSyncEventMapping();
            return connection.select(qSyncEventMapping.all()).from(qSyncEventMapping)
                    .where(qSyncEventMapping.REPO_ID.eq(repoId).and(qSyncEventMapping.ID.gt(maxIDinPreviousPage)))
                    .limit(PAGE_SIZE)
                    .orderBy(qSyncEventMapping.ID.asc())
                    .fetch();
        });
    }

    private Optional<Integer> getMaxIdIfPresent(List<Tuple> results) {
        final QSyncEventMapping qSyncEventMapping = new QSyncEventMapping();

        return results.stream()
                .map(t -> t.get(qSyncEventMapping.ID))
                .filter(Objects::nonNull)
                .reduce(Integer::max);
    }

    private SyncEvent getSyncEventFromTuple(final Tuple result) {
        final QSyncEventMapping qSyncEventMapping = new QSyncEventMapping();

        final Class<?> eventClass;
        try {
            eventClass = Class.forName(result.get(qSyncEventMapping.EVENT_CLASS));
            return (SyncEvent) new ObjectMapper().readValue(result.get(qSyncEventMapping.EVENT_JSON), eventClass);
        } catch (ClassNotFoundException e) {
            log.error("Unable to find the class that was stored in EVENT_CLASS %s ",
                    result.get(qSyncEventMapping.EVENT_CLASS),
                    e);
            throw new InvalidSyncEventInDatabase("Unable to find the class that was stored in event", e);
        } catch (IOException e) {
            log.error("Unable convert the stored json back into a SyncEvent %s ",
                    result.get(qSyncEventMapping.EVENT_JSON),
                    e);
            throw new InvalidSyncEventInDatabase("Unable convert the stored json back into a SyncEvent", e);
        }
    }

    private ContextAwareSyncEvent toContextAwareEvent(final Tuple result) {
        final QSyncEventMapping qSyncEventMapping = new QSyncEventMapping();

        return new DefaultContextAwareSyncEvent(
                result.get(qSyncEventMapping.ID),
                result.get(qSyncEventMapping.REPO_ID),
                ofNullable(result.get(qSyncEventMapping.SCHEDULED_SYNC)).orElse(false),
                getSyncEventFromTuple(result));
    }
}
