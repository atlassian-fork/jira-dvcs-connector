package com.atlassian.jira.plugins.dvcs.activeobjects.v3;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.Table;

/**
 * Table where we record which project keys have been successfully stored in Bitbucket
 */
@Preload
@Table("OrgToProject")
public interface OrganizationToProjectMapping extends Entity {
    String PROJECT_KEY = "PROJECT_KEY";
    String ORGANIZATION_ID = "ORGANIZATION_ID";

    String getProjectKey();

    void setProjectKey(String ProjectKey);

    @Indexed
    OrganizationMapping getOrganization();

    void setOrganization(OrganizationMapping repo);
}
