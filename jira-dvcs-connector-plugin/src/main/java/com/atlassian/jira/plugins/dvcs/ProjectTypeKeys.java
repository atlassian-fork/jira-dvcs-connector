package com.atlassian.jira.plugins.dvcs;

import com.atlassian.jira.project.type.ProjectTypeKey;

/**
 * This class holds the Software project type key constant.
 */
public final class ProjectTypeKeys {
    /**
     * The Software project type key constant.
     */
    public static final ProjectTypeKey SOFTWARE = new ProjectTypeKey("software");

    private ProjectTypeKeys() {
    }
}
