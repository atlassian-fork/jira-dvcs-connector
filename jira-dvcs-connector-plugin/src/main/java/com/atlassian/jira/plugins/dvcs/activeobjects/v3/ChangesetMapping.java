package com.atlassian.jira.plugins.dvcs.activeobjects.v3;

import net.java.ao.Entity;
import net.java.ao.ManyToMany;
import net.java.ao.OneToMany;
import net.java.ao.Preload;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

import java.util.Date;

@Preload
@Table("ChangesetMapping")
public interface ChangesetMapping extends Entity {

    String AUTHOR = "AUTHOR";
    String AUTHOR_EMAIL = "AUTHOR_EMAIL";
    String BRANCH = "BRANCH";
    String DATE = "DATE";
    String FILE_COUNT = "FILE_COUNT";
    String FILE_DETAILS_JSON = "FILE_DETAILS_JSON";
    /**
     * Rows at the table can contain data loaded by previous versions of this plugin. Some column data maybe missing because previous
     * versions of plugin was not loading them. To get the updated version of changeset we need to reload the data from the BB/GH servers.
     * This flag marks the row data as latest.
     */
    int LATEST_VERSION = 3;
    String MESSAGE = "MESSAGE";
    String NODE = "NODE";
    String PARENTS_DATA = "PARENTS_DATA";
    String RAW_AUTHOR = "RAW_AUTHOR";
    String RAW_NODE = "RAW_NODE";
    String SMARTCOMMIT_AVAILABLE = "SMARTCOMMIT_AVAILABLE";
    String TABLE_NAME = "AO_E8B6CC_CHANGESET_MAPPING";
    /**
     * Constant used to indicate that parents data could not be saved because they are too many
     */
    String TOO_MANY_PARENTS = "<TOO_MANY_PARENTS>";
    String VERSION = "VERSION";
    /**
     * FIXME: 2.0
     *
     * @deprecated was removed - kept as temporary state - can be removed in 2.0
     */
    @Deprecated
    String REPOSITORY_ID = "REPOSITORY_ID";
    /**
     * FIXME: 2.0
     *
     * @deprecated was removed - kept as temporary state - can be removed in 2.0
     */
    @Deprecated
    String PROJECT_KEY = "PROJECT_KEY";
    /**
     * FIXME: 2.0
     *
     * @deprecated was removed - kept as temporary state - can be removed in 2.0
     */
    @Deprecated
    String ISSUE_KEY = "ISSUE_KEY";
    /**
     * @deprecated as of 2.0.3
     */
    @Deprecated
    String FILES_DATA = "FILES_DATA";

    @ManyToMany(reverse = "getChangeset", through = "getRepository", value = RepositoryToChangesetMapping.class)
    RepositoryMapping[] getRepositories();

    @OneToMany(reverse = "getChangeset")
    IssueToChangesetMapping[] getIssues();

    @Indexed
    String getNode();

    void setNode(String node);

    String getRawAuthor();

    void setRawAuthor(String rawAuthor);

    @Indexed
    String getAuthor();

    void setAuthor(String author);

    Date getDate();

    void setDate(Date date);

    @Indexed
    String getRawNode();

    void setRawNode(String rawNode);

    String getBranch();

    void setBranch(String branch);

    @StringLength(StringLength.UNLIMITED)
    String getMessage();

    @StringLength(StringLength.UNLIMITED)
    void setMessage(String message);

    String getParentsData();

    void setParentsData(String parents);

    Integer getVersion();

    void setVersion(Integer version);

    String getAuthorEmail();

    void setAuthorEmail(String authorEmail);

    @Indexed
    Boolean isSmartcommitAvailable();

    /**
     * @since 2.0.3
     */
    int getFileCount();

    // Deprecated stuff

    /**
     * @since 2.0.3
     */
    void setFileCount(int fileCount);

    void setSmartcommitAvailable(Boolean available);

    @StringLength(StringLength.UNLIMITED)
    String getFileDetailsJson();

    void setFileDetailsJson(String fileDetailsJson);

    /**
     * @return {@link #REPOSITORY_ID}
     */
    @Deprecated
    int getRepositoryId();

    /**
     * @param repositoryId {@link #getRepositoryId()}
     */
    @Deprecated
    void setRepositoryId(int repositoryId);

    /**
     * @return {@link #PROJECT_KEY}
     */
    @Deprecated
    String getProjectKey();

    /**
     * @param projectKey {@link #getProjectKey()}
     */
    @Deprecated
    void setProjectKey(String projectKey);

    /**
     * @return {@link #ISSUE_KEY}
     */
    @Deprecated
    String getIssueKey();

    /**
     * @param issueKey {@link #getIssueKey()}
     */
    @Deprecated
    void setIssueKey(String issueKey);


    /**
     * @return
     * @deprecated as of 2.0.3, replaced by {@link #getFileDetailsJson()} and {@link #getFileCount()} instead
     */
    @Deprecated
    @StringLength(StringLength.UNLIMITED)
    String getFilesData();

    /**
     * @param files
     * @deprecated as of 2.0.3, replaced by {@link #setFileDetailsJson(String)} and {@link #setFileCount(int)} instead
     */
    @Deprecated
    @StringLength(StringLength.UNLIMITED)
    void setFilesData(String files);
}
