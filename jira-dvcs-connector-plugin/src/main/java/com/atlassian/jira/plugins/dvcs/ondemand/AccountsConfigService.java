package com.atlassian.jira.plugins.dvcs.ondemand;

public interface AccountsConfigService {
    /**
     * Schedules single reload, because AO is not available before
     * {@link com.atlassian.sal.api.lifecycle.LifecycleAware#onStart()}.
     */
    void scheduleReload();

    /**
     * Returns immediately, and reloads accounts configuration asynchronously (in separate thread).
     *
     * @see #reload()
     */
    void reloadAsync();

    /**
     * Reloads accounts configuration, and returns when it is done.
     *
     * @see #reloadAsync()
     */
    void reload();
}
