package com.atlassian.jira.plugins.dvcs.listener;

import com.atlassian.jira.plugins.dvcs.bitbucket.access.BitbucketTeamService;
import com.atlassian.jira.plugins.dvcs.model.Group;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicator;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicatorProvider;
import com.atlassian.jira.software.api.roles.LicenseService;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.stream.Collectors.toList;

/**
 * Invite users that are *not* created via the 'Create user' screen, e.g. cloud users creations via UM, importing users
 * using JIM, users created from Service Desk screens (or screens that belong to other plugins).
 * <p>
 * This class differs from {@link com.atlassian.jira.plugins.dvcs.listener.UserAddedViaInterfaceEventProcessor} in the way
 * it determines the teams and groups that users will be invited to. Instead of relying on the administrator's selection
 * (which is non-existent in this case), it looks up all Bitbucket teams with default groups.
 */
@Component
public class UserAddedExternallyEventProcessor {
    @VisibleForTesting
    static final String DVCS_TYPE_BITBUCKET = "bitbucket";
    /**
     * BBC-957: Attribute key to recognise Service Desk Customers during user creation
     */
    @VisibleForTesting
    static final String SERVICE_DESK_CUSTOMERS_ATTRIBUTE_KEY = "synch.servicedesk.requestor";
    private static final Logger LOGGER = LoggerFactory.getLogger(UserAddedExternallyEventProcessor.class);
    private final BitbucketTeamService bitbucketTeamService;

    private final DvcsCommunicatorProvider dvcsCommunicatorProvider;

    private final LicenseService licenseService;

    @Autowired
    public UserAddedExternallyEventProcessor(
            @ComponentImport final LicenseService licenseService,
            final BitbucketTeamService bitbucketTeamService,
            final DvcsCommunicatorProvider dvcsCommunicatorProvider) {
        this.bitbucketTeamService = checkNotNull(bitbucketTeamService);
        this.dvcsCommunicatorProvider = checkNotNull(dvcsCommunicatorProvider);
        this.licenseService = checkNotNull(licenseService);
    }

    public void process(final ApplicationUser user) {
        checkArgument(user != null, "Expecting user to be non-null");

        if (shouldInvite(user)) {
            invite(user);
        }
    }

    private boolean shouldInvite(final ApplicationUser user) {
        return licenseService.isSoftwareUser(user);
    }

    private void invite(final ApplicationUser user) {
        final DvcsCommunicator dvcsCommunicator = dvcsCommunicatorProvider.getCommunicator(DVCS_TYPE_BITBUCKET);
        for (final Organization bitbucketTeam : bitbucketTeamService.getTeamsWithDefaultGroups()) {
            final Collection<String> groups = groupNames(bitbucketTeam.getDefaultGroups());
            LOGGER.debug("Inviting user {} to groups {} in organization {}",
                    new Object[]{user.getUsername(), groups, bitbucketTeam.getName()});

            dvcsCommunicator.inviteUser(bitbucketTeam, groups, user.getEmailAddress());
        }
    }

    private Collection<String> groupNames(final Collection<Group> groups) {
        return groups.stream().map(Group::getSlug).collect(toList());
    }
}
