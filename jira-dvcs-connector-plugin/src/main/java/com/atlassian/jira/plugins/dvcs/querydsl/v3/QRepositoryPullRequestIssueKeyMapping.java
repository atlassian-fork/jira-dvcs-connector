package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.SimpleExpression;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.PrimaryKey;

public class QRepositoryPullRequestIssueKeyMapping extends EnhancedRelationalPathBase<QRepositoryPullRequestIssueKeyMapping>
        implements IssueKeyedMapping {
    private static final String AO_TABLE_NAME = "AO_E8B6CC_PR_ISSUE_KEY";
    private static final long serialVersionUID = 5357146316056312048L;

    public final NumberPath<Integer> ID = createInteger("ID");
    public final StringPath ISSUE_KEY = createString("ISSUE_KEY");
    public final NumberPath<Integer> PULL_REQUEST_ID = createInteger("PULL_REQUEST_ID");
    public final PrimaryKey<QRepositoryPullRequestIssueKeyMapping> PR_ISSUE_KEY_PK = createPrimaryKey(ID);

    public QRepositoryPullRequestIssueKeyMapping() {
        super(QRepositoryPullRequestIssueKeyMapping.class, AO_TABLE_NAME);
    }

    @Override
    public SimpleExpression getIssueKeyExpression() {
        return ISSUE_KEY;
    }
}