package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.jira.plugins.dvcs.dao.OrganizationDao;
import com.atlassian.jira.plugins.dvcs.dao.RepositoryDao;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicator;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicatorProvider;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableSet;
import io.atlassian.fugue.Unit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;

@ParametersAreNonnullByDefault
@Named
public class LinkerServiceImpl implements LinkerService {

    @VisibleForTesting
    static final String ORG_LINKER_VALUE_CLUSTER_LOCK_BASE
            = "com.atlassian.jira.plugins.dvcs.service.organization.linkerValue.";
    private static final Logger LOG = LoggerFactory.getLogger(LinkerServiceImpl.class);
    private final ClusterLockService clusterLockService;

    private final ProjectManager projectManager;

    private final DvcsCommunicatorProvider dvcsCommunicatorProvider;

    private final OrganizationDao organizationDao;
    private final RepositoryDao repositoryDao;
    private final DvcsConnectorExecutorFactory executorFactory;
    private ThreadPoolExecutor executor;

    @Inject
    public LinkerServiceImpl(@ComponentImport final ClusterLockService clusterLockService,
                             @ComponentImport final ProjectManager projectManager,
                             final DvcsCommunicatorProvider dvcsCommunicatorProvider,
                             final DvcsConnectorExecutorFactory executorFactory,
                             final OrganizationDao organizationDao,
                             final RepositoryDao repositoryDao) {
        this.clusterLockService = requireNonNull(clusterLockService);
        this.projectManager = requireNonNull(projectManager);
        this.dvcsCommunicatorProvider = requireNonNull(dvcsCommunicatorProvider);
        this.organizationDao = requireNonNull(organizationDao);
        this.repositoryDao = requireNonNull(repositoryDao);

        this.executorFactory = requireNonNull(executorFactory);
        this.executor = executorFactory.createLinkerServiceThreadPoolExecutor();
    }

    @PreDestroy
    public void destroy() {
        executorFactory.shutdownExecutor(LinkerServiceImpl.class.getName(), executor);
    }

    @Override
    public Future<Unit> updateConnectLinkerValuesAsync(int organizationId) {
        LOG.debug("Triggering async processing of removeLinkers for organizationId: {}", organizationId);
        return executor.submit(() -> updateConnectLinkerValues(organizationId));
    }

    @Override
    public Unit updateConnectLinkerValues(int organizationId) {
        final Organization organization = organizationDao.get(organizationId);

        final Optional<PrincipalIDCredential> maybePrincipal
                = organization.getCredential().accept(PrincipalIDCredential.visitor());

        if (!maybePrincipal.isPresent()) {
            return Unit.Unit();
        }

        final ClusterLock lock = clusterLockService.getLockForName(ORG_LINKER_VALUE_CLUSTER_LOCK_BASE + organizationId);
        boolean lockAcquired = false;
        try {
            lockAcquired = lock.tryLock(1, TimeUnit.MINUTES);
            if (!lockAcquired) {
                // don't worry about updating, some other sync is updating it already and is holding the lock
                return Unit.Unit();
            }

            final Collection<String> projectKeysOnChangesets
                    = organizationDao.getAllProjectKeysFromChangesetsInOrganization(organizationId);

            final Iterable<String> currentlyLinkedProjectsIterable
                    = organizationDao.getCurrentlyLinkedProjects(organizationId);
            final Set<String> currentlyLinkedProjects
                    = ImmutableSet.<String>builder().addAll(currentlyLinkedProjectsIterable).build();
            final Set<String> projectKeysInJira = getProjectKeysInJira();

            projectKeysInJira.retainAll(projectKeysOnChangesets);

            if (!projectKeysInJira.equals(currentlyLinkedProjects)) {
                LOG.debug("project keys have changed, updating Bitbucket with {}", projectKeysInJira);
                final DvcsCommunicator communicator = dvcsCommunicatorProvider.getCommunicator(organization.getDvcsType());
                communicator.setConnectLinkerValuesForOrganization(organization, projectKeysInJira);
                organizationDao.updateLinkedProjects(organizationId, projectKeysInJira);
            }
        } catch (InterruptedException e) {
            // doesn't matter. if linkers fail this time we will update them on the next sync or some other
            // repository is already updating them
        } finally {
            if (lock != null && lockAcquired) {
                lock.unlock();
            }
        }
        return Unit.Unit();
    }

    private Set<String> getProjectKeysInJira() {
        final Stream<Project> projectObjects = projectManager.getProjectObjects().stream();
        return projectObjects.map(project -> project.getKey()).collect(Collectors.toSet());
    }

    @Override
    public Unit removeLinkers(@Nonnull final Organization organization) {
        LOG.debug("Removing linkers for Organization id: {}, name: {}", organization.getId(), organization.getName());
        final DvcsCommunicator communicator = dvcsCommunicatorProvider.getCommunicator(organization.getDvcsType());
        List<Repository> repositories = repositoryDao.getAllByOrganization(organization.getId(), false);
        repositories.forEach(repository -> communicator.removeLinkersForRepo(repository));
        LOG.debug("Finished removing linkers for Organization id: {}, name: {}",
                organization.getId(),
                organization.getName());
        return Unit.Unit();
    }

    @Override
    public Future<Unit> removeLinkersAsync(@Nonnull final Organization organization) {
        LOG.debug("Triggering async processing of removeLinkers for orgId: {}", organization.getId());
        return executor.submit(() -> removeLinkers(organization));
    }
}
