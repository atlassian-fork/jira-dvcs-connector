package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl.util;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * This class offers a convenience method to replace the use of DB streams.
 */
public class PseudoStream {

    /**
     * This method applies a function to all records in a table, this is can be used as a replacement for the deprecated
     * AO.Stream.
     * <p>
     * This method is treating the underlying table as being sorted by ID in ascending order,
     * so if this invariant is broken the method will not behave as expected.
     * <p>
     * This method fetches results a page at a time and then applies the consumer function to each record in the page
     * before fetching a new page of results.
     * <p>
     * This method terminates when getPageMaxIndex returns an empty, which should be when the list of
     * results it was passed is either empty or contains no ID numbers.
     *
     * @param startingIndexSupplier Supplies the starting index value that all results will have a value greater than.
     * @param rowPageSupplier       a function that returns a page of results that have an ID greater than the argument,
     *                              where the entire table has been put into ascending order by the ID.
     * @param getPageMaxIndex       the Optional maximum Index contained within the argument. If the argument is empty then this returns empty.
     * @param rowToModelConverter   A function that maps a T to a model class suitable for exposing outside of a DAO
     * @param modelConsumer         A consumer function we want to apply to each record in the table
     * @param <M>                   The type of the model class we are wanting to consume
     * @param <T>                   The type of the result we get from our getPage function
     * @param <I>                   The type of the index we are using to order results
     */
    public static <I, T, M> void consumeAllInTable(
            Supplier<I> startingIndexSupplier,
            Function<I, List<T>> rowPageSupplier,
            Function<List<T>, Optional<I>> getPageMaxIndex,
            Function<T, M> rowToModelConverter,
            Consumer<M> modelConsumer) {

        Optional<I> startId = Optional.of(startingIndexSupplier.get());
        while (startId.isPresent()) {
            List<T> pageOfResults = rowPageSupplier.apply(startId.get());
            pageOfResults.stream()
                    .map(rowToModelConverter::apply)
                    .forEach(modelConsumer::accept);
            startId = getPageMaxIndex.apply(pageOfResults);
        }
    }

    /**
     * Gets the maximum Integer contained in the list, if it contains any elements
     *
     * @param values
     * @return the maximum value if the list was non empty, else none
     */
    public static Optional<Integer> getMax(final Collection<Integer> values) {
        return values.stream().reduce(Integer::max);
    }
}
