package com.atlassian.jira.plugins.dvcs.rest;

import com.atlassian.jira.exception.NotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
public class ExceptionMapper implements javax.ws.rs.ext.ExceptionMapper<Exception> {

    @Override
    public Response toResponse(final Exception e) {
        if (e instanceof NotFoundException) {
            return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
        }
        if (e instanceof IllegalStateException) {
            return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
        }
        if (e instanceof IllegalArgumentException) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        }
        return Response.serverError().build();
    }
}
