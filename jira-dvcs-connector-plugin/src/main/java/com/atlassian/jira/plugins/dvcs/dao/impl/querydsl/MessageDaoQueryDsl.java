package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.jira.plugins.dvcs.dao.MessageDao;
import com.atlassian.jira.plugins.dvcs.dao.impl.MessageAoDao;
import com.atlassian.jira.plugins.dvcs.dao.impl.querydsl.util.PseudoStream;
import com.atlassian.jira.plugins.dvcs.dao.impl.transform.MessageEntityTransformer;
import com.atlassian.jira.plugins.dvcs.model.Message;
import com.atlassian.jira.plugins.dvcs.model.MessageId;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QMessageMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QMessageQueueItemMapping;
import com.atlassian.jira.plugins.dvcs.querydsl.v3.QMessageTagMapping;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import org.slf4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.slf4j.LoggerFactory.getLogger;

@Named
@ParametersAreNonnullByDefault
public class MessageDaoQueryDsl implements MessageDao {
    private static final int PAGE_SIZE = 1000;
    private static final Logger log = getLogger(MessageDaoQueryDsl.class);
    private final MessageAoDao messageAoDao;
    private final MessageEntityTransformer messageEnitityTransformer;
    private final DatabaseAccessor databaseAccessor;

    @Inject
    public MessageDaoQueryDsl(final DatabaseAccessor databaseAccessor,
                              final MessageAoDao messageAoDao,
                              final MessageEntityTransformer messageEnitityTransformer) {
        this.databaseAccessor = checkNotNull(databaseAccessor);
        this.messageAoDao = checkNotNull(messageAoDao);
        this.messageEnitityTransformer = checkNotNull(messageEnitityTransformer);
    }

    @Override
    @Nonnull
    public Message create(final Map<String, Object> message, final String[] tags) {
        return messageEnitityTransformer.toMessage(messageAoDao.create(message, tags));
    }

    @Override
    public void createTag(final Message message, final String tag) {
        messageAoDao.createTag(message.getId(), tag);
    }

    @Override
    public String[] getTags(final MessageId messageId) {
        final List<String> tags = databaseAccessor.runInTransaction(connection -> {
            final QMessageTagMapping qMessageTagMapping = new QMessageTagMapping();
            return connection.select(qMessageTagMapping.TAG)
                    .from(qMessageTagMapping)
                    .where(qMessageTagMapping.MESSAGE_ID.eq(messageId.getId()))
                    .fetch();
        });

        return tags.toArray(new String[tags.size()]);
    }

    @Override
    public void delete(final Message message) {
        databaseAccessor.runInTransaction(connection -> {
            final QMessageTagMapping qMessageTagMapping = new QMessageTagMapping();
            final QMessageMapping qMessageMapping = new QMessageMapping();

            connection.delete(qMessageTagMapping)
                    .where(qMessageTagMapping.MESSAGE_ID.eq(message.getId()))
                    .execute();

            return connection.delete(qMessageMapping)
                    .where(qMessageMapping.ID.eq(message.getId()))
                    .execute();
        });
    }

    @Override
    public void deleteTag(final Message message, final String tag) {
        databaseAccessor.runInTransaction(connection -> {
            final QMessageTagMapping qMessageTagMapping = new QMessageTagMapping();

            return connection.delete(qMessageTagMapping)
                    .where(qMessageTagMapping.MESSAGE_ID.eq(message.getId()).and(qMessageTagMapping.TAG.eq(tag)))
                    .execute();
        });
    }

    @Override
    @Nonnull
    public Message getById(int id) {
        return messageEnitityTransformer.toMessage(messageAoDao.getById(id));
    }

    @Override
    public int getMessagesForConsumingCount(final String tag) {
        return messageAoDao.getMessagesForConsumingCount(tag);
    }

    @Override
    public void getByTag(final String tag, final Consumer<MessageId> messagesMappingConsumer) {
        PseudoStream.consumeAllInTable(
                () -> getInitialIndex(tag),
                id -> getPageOfResults(tag, id),
                PseudoStream::getMax,
                MessageId::new,
                messagesMappingConsumer::accept);
    }

    /**
     * Gets the largest index that is smaller than the minimum index present in the result set
     * ie one less that smallest ID found in the table
     *
     * @param tag the tag we are selecting by
     * @return the largest index that is less than the min index found in the table
     */
    private int getInitialIndex(final String tag) {
        return databaseAccessor.runInTransaction(connection -> {
            final QMessageQueueItemMapping qMessageQueueItemMapping = new QMessageQueueItemMapping();
            final QMessageTagMapping qMessageTagMapping = new QMessageTagMapping();
            final QMessageMapping qMessageMapping = new QMessageMapping();

            return connection.select(qMessageMapping.ID.min()).from(qMessageMapping)
                    .leftJoin(qMessageTagMapping)
                    .on(qMessageMapping.ID.eq(qMessageTagMapping.MESSAGE_ID))
                    .leftJoin(qMessageQueueItemMapping)
                    .on(qMessageMapping.ID.eq(qMessageQueueItemMapping.MESSAGE_ID))
                    .where(qMessageTagMapping.TAG.eq(tag))
                    .fetch();
        })
                .stream()
                .map(Optional::ofNullable)
                .findFirst().get()
                .orElse(0) - 1;
    }

    /**
     * Gets a page of results that match the tag and have an MessageMappingID greater than initialIdForPage.
     * The is the next page when sorting asc by MessageMappingID
     *
     * @param tag              the tag to match on
     * @param initialIdForPage the Id number we want all results to have an ID greater than
     * @return a page of MessageMappingID ID numbers that match the query
     */
    private List<Integer> getPageOfResults(final String tag, final int initialIdForPage) {
        return databaseAccessor.runInTransaction(connection -> {
            final QMessageTagMapping qMessageTagMapping = new QMessageTagMapping();
            final QMessageMapping qMessageMapping = new QMessageMapping();

            return connection
                    .select(qMessageMapping.ID)
                    .from(qMessageMapping)
                    .limit(PAGE_SIZE)
                    .leftJoin(qMessageTagMapping)
                    .on(qMessageMapping.ID.eq(qMessageTagMapping.MESSAGE_ID))
                    .where(qMessageTagMapping.TAG.eq(tag)
                            .and(qMessageMapping.ID.gt(initialIdForPage)))
                    .orderBy(qMessageMapping.ID.asc())
                    .distinct()
                    .fetch();
        });
    }
}
