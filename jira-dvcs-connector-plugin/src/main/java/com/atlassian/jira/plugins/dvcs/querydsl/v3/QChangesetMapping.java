package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import java.util.Date;


public class QChangesetMapping extends EnhancedRelationalPathBase<QChangesetMapping> {
    private static final String AO_TABLE_NAME = "AO_E8B6CC_CHANGESET_MAPPING";
    private static final long serialVersionUID = 4748645041052511906L;

    public final StringPath AUTHOR = createString("AUTHOR");
    public final StringPath AUTHOR_EMAIL = createString("AUTHOR_EMAIL");
    public final StringPath BRANCH = createString("BRANCH");
    public final DateTimePath<Date> DATE = createDateTime("DATE", Date.class);
    public final StringPath FILES_DATA = createString("FILES_DATA");
    public final NumberPath<Integer> FILE_COUNT = createInteger("FILE_COUNT");
    public final StringPath FILE_DETAILS_JSON = createString("FILE_DETAILS_JSON");
    public final NumberPath<Integer> ID = createInteger("ID");
    public final StringPath ISSUE_KEY = createString("ISSUE_KEY");
    public final StringPath MESSAGE = createString("MESSAGE");
    public final StringPath NODE = createString("NODE");
    public final StringPath PARENTS_DATA = createString("PARENTS_DATA");
    public final StringPath PROJECT_KEY = createString("PROJECT_KEY");
    public final StringPath RAW_AUTHOR = createString("RAW_AUTHOR");
    public final StringPath RAW_NODE = createString("RAW_NODE");
    public final NumberPath<Integer> REPOSITORY_ID = createInteger("REPOSITORY_ID");
    public final NumberPath<Integer> VERSION = createInteger("VERSION");
    public final BooleanPath SMARTCOMMIT_AVAILABLE = createBoolean("SMARTCOMMIT_AVAILABLE");
    public final com.querydsl.sql.PrimaryKey<QChangesetMapping> CHANGESETMAPPING_PK = createPrimaryKey(ID);

    public QChangesetMapping() {
        super(QChangesetMapping.class, AO_TABLE_NAME);
    }

}