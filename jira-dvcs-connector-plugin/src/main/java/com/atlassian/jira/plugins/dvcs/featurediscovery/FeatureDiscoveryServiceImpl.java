package com.atlassian.jira.plugins.dvcs.featurediscovery;

import com.atlassian.jira.plugins.dvcs.util.ExceptionLogger;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.usersettings.UserSettingsBuilder;
import com.atlassian.sal.api.usersettings.UserSettingsService;
import org.slf4j.Logger;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import javax.inject.Named;

import static java.util.Objects.requireNonNull;

@Named
@ParametersAreNonnullByDefault
public class FeatureDiscoveryServiceImpl implements FeatureDiscoveryService {

    public static final String FEATURE_DISCOVERY_USER_SETTINGS_KEY = "com.atlassian.jira.plugins.dvcs.featurediscovery";
    private static final Logger log = ExceptionLogger.getLogger(FeatureDiscoveryServiceImpl.class);

    private final UserManager userManager;
    private final UserSettingsService userSettingsService;

    @Inject
    public FeatureDiscoveryServiceImpl(@ComponentImport("salUserManager") final UserManager userManager,
                                       @ComponentImport final UserSettingsService userSettingsService) {
        this.userManager = requireNonNull(userManager);
        this.userSettingsService = requireNonNull(userSettingsService);
    }

    @Override
    public boolean hasUserSeenFeatureDiscovery() {
        final UserKey user = userManager.getRemoteUserKey();
        if (user == null) {
            log.debug("No logged in user found - cannot retrieve feature discovery flag.");
            return true;
        }

        try {
            return userSettingsService
                    .getUserSettings(user)
                    .getBoolean(FEATURE_DISCOVERY_USER_SETTINGS_KEY)
                    .getOrElse(Boolean.FALSE);
        }
        catch (Exception e) {
            log.debug("Exception occurred while retrieving feature discovery flag from user settings.", e);
            return true;
        }
    }

    @Override
    public void markUserAsHavingSeenFeatureDiscovery(final boolean hasSeenFeatureDiscovery) {
        final UserKey user = userManager.getRemoteUserKey();
        if (user == null) {
            log.debug("No logged in user found - cannot update feature discovery flag.");
            return;
        }

        try {
            userSettingsService.updateUserSettings(user, (UserSettingsBuilder b) -> {
                        return b.put(FEATURE_DISCOVERY_USER_SETTINGS_KEY, hasSeenFeatureDiscovery).build();
                    }
            );
        }
        catch (Exception e) {
            log.debug("Exception occurred while updating feature discovery flag in user settings.", e);
        }
    }
}
