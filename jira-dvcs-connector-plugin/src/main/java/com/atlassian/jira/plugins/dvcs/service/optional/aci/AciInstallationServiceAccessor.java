package com.atlassian.jira.plugins.dvcs.service.optional.aci;

import com.atlassian.fusion.aci.api.service.ACIInstallationService;
import com.atlassian.jira.plugins.dvcs.service.optional.ServiceAccessor;
import com.atlassian.pocketknife.api.lifecycle.services.OptionalService;
import com.atlassian.pocketknife.spi.lifecycle.services.OptionalServiceAccessor;
import org.osgi.framework.BundleContext;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;

@Named
public class AciInstallationServiceAccessor extends OptionalServiceAccessor<ACIInstallationService>
        implements ServiceAccessor<ACIInstallationService> {
    @Inject
    public AciInstallationServiceAccessor(final BundleContext bundleContext) {
        super(bundleContext, ACIInstallationService.class.getName());
    }

    @Override
    public Optional<ACIInstallationService> get() {
        final OptionalService<ACIInstallationService> optionalService = obtain();
        return optionalService.isAvailable() ? of(optionalService.get()) : empty();
    }
}