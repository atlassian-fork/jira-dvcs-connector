package com.atlassian.jira.plugins.dvcs.scheduler;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.service.message.MessagingService;
import com.atlassian.jira.plugins.dvcs.sync.SyncConfig;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static com.atlassian.scheduler.config.Schedule.forInterval;
import static com.google.common.base.Preconditions.checkNotNull;

@Component
@ExportAsService(LifecycleAware.class)
public class DvcsScheduler implements LifecycleAware {
    @VisibleForTesting
    static final JobId JOB_ID = JobId.of(DvcsScheduler.class.getName() + ":job");

    @VisibleForTesting
    static final JobRunnerKey JOB_RUNNER_KEY = JobRunnerKey.of(DvcsScheduler.class.getName());

    private static final Logger log = LoggerFactory.getLogger(DvcsScheduler.class);

    private final ActiveObjects activeObjects;
    private final DvcsSchedulerJobRunner dvcsSchedulerJobRunner;
    private final MessagingService messagingService;
    private final SchedulerService schedulerService;
    private final SyncConfig syncConfig;

    @Autowired
    public DvcsScheduler(@ComponentImport final ActiveObjects activeObjects,
                         @ComponentImport final SchedulerService schedulerService,
                         final DvcsSchedulerJobRunner dvcsSchedulerJobRunner,
                         final MessagingService messagingService,
                         final SyncConfig syncConfig) {
        this.activeObjects = checkNotNull(activeObjects);
        this.dvcsSchedulerJobRunner = checkNotNull(dvcsSchedulerJobRunner);
        this.messagingService = checkNotNull(messagingService);
        this.schedulerService = checkNotNull(schedulerService);
        this.syncConfig = checkNotNull(syncConfig);
    }

    public void onStart() {
        try {
            scheduleJob();
        } catch (final SchedulerServiceException ex) {
            log.error("Unexpected error during launch", ex);
        }

        new Thread(() -> {
            try {
                activeObjects.moduleMetaData().awaitInitialization();
                messagingService.onStart();
            } catch (final Exception e) {
                log.error("Exception waiting for AO to initialize", e);
            }
        }, "WaitForAO").start();
    }

    @Override
    public void onStop() {
        schedulerService.unregisterJobRunner(JOB_RUNNER_KEY);
        log.info("DvcsScheduler job handler unregistered");
    }

    @VisibleForTesting
    void scheduleJob() throws SchedulerServiceException {
        // always register the job handler, regardless of whether the job has been scheduled
        schedulerService.registerJobRunner(JOB_RUNNER_KEY, dvcsSchedulerJobRunner);
        log.info("DvcsScheduler job handler registered");

        // only schedule the job when it's not already scheduled
        if (schedulerService.getJobDetails(JOB_ID) == null) {
            schedulerService.scheduleJob(JOB_ID, getJobConfig());
        }
    }

    private JobConfig getJobConfig() {
        final long interval = syncConfig.scheduledSyncIntervalMillis();
        final long randomStartTimeWithinInterval = new Date().getTime() + (long) (new Random().nextDouble() * interval);
        final Date startTime = new Date(randomStartTimeWithinInterval);
        log.info("DvcsScheduler start planned at " + startTime + ", interval=" + interval);
        return JobConfig
                .forJobRunnerKey(JOB_RUNNER_KEY)
                .withRunMode(RUN_ONCE_PER_CLUSTER)
                .withSchedule(forInterval(interval, startTime));
    }
}
