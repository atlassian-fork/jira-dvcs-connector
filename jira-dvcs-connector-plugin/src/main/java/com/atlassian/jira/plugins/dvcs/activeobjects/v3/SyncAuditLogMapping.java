package com.atlassian.jira.plugins.dvcs.activeobjects.v3;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

import java.util.Date;

@Preload
@Table("SyncAuditLog")
public interface SyncAuditLogMapping extends Entity {
    String SYNC_STATUS_RUNNING = "RUNNING";
    String SYNC_STATUS_FAILED = "FAILED";
    String SYNC_STATUS_SUCCESS = "SUCCESS";
    String SYNC_STATUS_SLEEPING = "SLEEPING";
    String SYNC_TYPE_SOFT = "SOFT";
    String SYNC_TYPE_CHANGESETS = "CHANGESETS";
    String SYNC_TYPE_PULLREQUESTS = "PULLREQUESTS";
    String SYNC_TYPE_WEBHOOKS = "WEBHOOKS";

    //
    String REPO_ID = "REPO_ID";
    String START_DATE = "START_DATE";
    String END_DATE = "END_DATE";
    String SYNC_STATUS = "SYNC_STATUS";
    String EXC_TRACE = "EXC_TRACE";
    String SYNC_TYPE = "SYNC_TYPE"; // hard, soft
    String TOTAL_ERRORS = "TOTAL_ERRORS";

    @Indexed
    int getRepoId();

    void setRepoId(int id);

    Date getStartDate();

    void setStartDate(Date date);

    Date getEndDate();

    void setEndDate(Date date);

    String getSyncStatus();

    void setSyncStatus(String status);

    int getFlightTimeMs();

    void setFlightTimeMs(int flightTime);

    Date getFirstRequestDate();

    void setFirstRequestDate(Date firstRequestDate);

    int getNumRequests();

    void setNumRequests(int numRequests);

    String getSyncType();

    void setSyncType(String type);

    @StringLength(StringLength.UNLIMITED)
    String getExcTrace();

    @StringLength(StringLength.UNLIMITED)
    void setExcTrace(String trace);

    int getTotalErrors();

    void setTotalErrors(int total);

}
