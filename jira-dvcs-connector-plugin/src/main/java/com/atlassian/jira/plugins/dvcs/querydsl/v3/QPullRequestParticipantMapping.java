package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.jira.plugins.dvcs.activity.PullRequestParticipantMapping;
import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

public class QPullRequestParticipantMapping extends EnhancedRelationalPathBase<QPullRequestParticipantMapping> {

    private static final String AO_TABLE_NAME = "AO_E8B6CC_PR_PARTICIPANT";
    private static final long serialVersionUID = -6375419004292380634L;

    public final NumberPath<Integer> ID = createNumber("ID", Integer.class);
    public final StringPath ROLE = createString("ROLE");
    public final StringPath USERNAME = createString("USERNAME");
    public final BooleanPath APPROVED = createBoolean(PullRequestParticipantMapping.APPROVED);
    public final NumberPath<Integer> PULL_REQUEST_ID = createInteger(PullRequestParticipantMapping.PULL_REQUEST_ID);
    public final com.querydsl.sql.PrimaryKey<QPullRequestParticipantMapping> PR_PARTICIPANT_PK = createPrimaryKey(ID);

    public QPullRequestParticipantMapping() {
        super(QPullRequestParticipantMapping.class, AO_TABLE_NAME);
    }
}