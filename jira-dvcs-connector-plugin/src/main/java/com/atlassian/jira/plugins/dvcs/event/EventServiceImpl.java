package com.atlassian.jira.plugins.dvcs.event;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.util.concurrent.ThreadFactories;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.PreDestroy;
import javax.annotation.concurrent.Immutable;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.util.concurrent.ThreadFactories.Type.DAEMON;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.concurrent.TimeUnit.SECONDS;

@Component
public class EventServiceImpl implements EventService {
    private static final Logger logger = LoggerFactory.getLogger(EventServiceImpl.class);
    private static final int DESTROY_TIMEOUT_SECS = 10;

    private final EventPublisher eventPublisher;
    private final SyncEventDao syncEventDao;
    private final EventLimiterFactory eventLimiterFactory;
    private final ThreadPoolExecutor eventDispatcher;

    @Autowired
    public EventServiceImpl(@ComponentImport final EventPublisher eventPublisher,
                            final EventLimiterFactory eventLimiterFactory,
                            final SyncEventDao syncEventDao) {
        this(eventPublisher, syncEventDao, eventLimiterFactory, createEventDispatcher());
    }

    @VisibleForTesting
    EventServiceImpl(@Nonnull final EventPublisher eventPublisher,
                     @Nonnull final SyncEventDao syncEventDao,
                     @Nonnull final EventLimiterFactory eventLimiterFactory,
                     @Nonnull final ThreadPoolExecutor executorService) {
        this.eventPublisher = checkNotNull(eventPublisher);
        this.syncEventDao = checkNotNull(syncEventDao);
        this.eventLimiterFactory = checkNotNull(eventLimiterFactory);
        this.eventDispatcher = checkNotNull(executorService);
    }

    private static ThreadPoolExecutor createEventDispatcher() {
        return ThreadPoolUtil.newSingleThreadExecutor(ThreadFactories
                .named("DVCSConnector.EventService")
                .type(DAEMON)
                .build()
        );
    }

    @Override
    public void storeEvent(Repository repository, SyncEvent event, boolean scheduledSync) throws IllegalArgumentException {
        storeEvent(repository.getId(), event, scheduledSync);
    }

    @Override
    public void storeEvent(int repositoryId, SyncEvent event, boolean scheduledSync) throws IllegalArgumentException {
        storeEvent(new DefaultContextAwareSyncEvent(repositoryId, scheduledSync, event));
    }

    @Override
    public void storeEvent(ContextAwareSyncEvent event) throws IllegalArgumentException {
        syncEventDao.save(event);
    }

    @Override
    public void dispatchEvents(Repository repository) {
        dispatch(new DispatchRequest(repository));
    }

    @Override
    public void dispatchEvents(int repositoryId) {
        doDispatchEvents(new DispatchRequest(repositoryId, "repository with id " + repositoryId));
    }

    private void dispatch(final DispatchRequest dispatchRequest) {
        // do the event dispatching asynchronously
        eventDispatcher.submit(() -> {
            try {
                doDispatchEvents(dispatchRequest);
                return null;
            } catch (RuntimeException e) {
                logger.error("Error dispatching events for: " + dispatchRequest, e);
                throw new RuntimeException(e);
            }
        });
    }

    /**
     * Shuts down the event dispatcher pool. This method waits for up to {@link #DESTROY_TIMEOUT_SECS} for the executor
     * to shut down before logging an error and returning.
     */
    @PreDestroy
    public void destroy() {
        destroyEventDispatcher();
    }

    /**
     * Dispatches all events for a repo. If the thread running this method is interrupted while this method is
     * dispatching events this method will return early, preserving the thread's interrupted status.
     *
     * @param dispatch a DispatchRequest
     */
    private void doDispatchEvents(final DispatchRequest dispatch) {
        final EventLimiter limiter = eventLimiterFactory.create();

        final AtomicInteger dispatched = new AtomicInteger(0);
        syncEventDao.foreachByRepoId(dispatch.repoId(), contextAwareEvent -> {
            if (Thread.interrupted()) {
                logger.error("Thread interrupted after dispatching {} events for: {}", new Object[]{dispatched.get(), dispatch});
                Thread.currentThread().interrupt();
                return;
            }
            try {
                final SyncEvent event = contextAwareEvent.getSyncEvent();
                if (limiter.isLimitExceeded(event, contextAwareEvent.scheduledSync())) {
                    logger.debug("Limit exceeded, dropping event for repository {}: {}", dispatch, event);
                    return;
                }
                logger.debug("Publishing event for repository {}: {}", dispatch, event);
                eventPublisher.publish(event);
                dispatched.incrementAndGet();
            } finally {
                syncEventDao.delete(contextAwareEvent);
            }
        });

        int dropped = limiter.getLimitExceededCount();
        if (dropped > 0) {
            logger.info("Event limit exceeded for {}. Dropped {} subsequent events.", dispatch, dropped);
            eventPublisher.publish(new LimitExceededEvent(dropped));
        }
    }

    @Override
    public void discardEvents(Repository repository) {
        long deleted = syncEventDao.deleteAll(repository.getId());
        logger.debug("Deleted {} events from repo: {}", deleted, repository);
    }

    private void destroyEventDispatcher() {
        eventDispatcher.shutdown();
        eventDispatcher.getQueue().clear();
        try {
            boolean destroyed = eventDispatcher.awaitTermination(DESTROY_TIMEOUT_SECS, SECONDS);
            if (!destroyed) {
                logger.error("ExecutorService did not shut down within {}s", DESTROY_TIMEOUT_SECS);
            }
        } catch (InterruptedException e) {
            logger.error("Interrupted while waiting for ExecutorService to shut down.");
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Parameter object for dispatching events. Makes it easier to provide a sane toString() in logging, etc.
     */
    @Immutable
    private static class DispatchRequest {
        private final int repoId;
        private final String repoToString;

        public DispatchRequest(@Nonnull Repository repository) {
            this.repoId = repository.getId();
            this.repoToString = repository.toString();
        }

        private DispatchRequest(final int repoId, final String repoToString) {
            this.repoId = repoId;
            this.repoToString = repoToString;
        }

        public int repoId() {
            return repoId;
        }

        @Override
        public String toString() {
            return String.format("Repository[%s]", repoToString);
        }
    }
}
