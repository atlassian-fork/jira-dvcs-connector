package com.atlassian.jira.plugins.dvcs.dao;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageMapping;
import com.atlassian.jira.plugins.dvcs.model.Message;
import com.atlassian.jira.plugins.dvcs.model.MessageId;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Map;
import java.util.function.Consumer;

/**
 * DAO layer for Messages
 */
@ParametersAreNonnullByDefault
public interface MessageDao {

    /**
     * Creates new {@link MessageMapping} for provided parameters.
     *
     * @param message the map we are creating an record for
     * @param tags    the tags to associate with the new record
     * @return created {@link MessageMapping} entity
     */
    @Nonnull
    Message create(Map<String, Object> message, String[] tags);

    /**
     * Creates a new tag associated with a message
     *
     * @param message the message we are adding a tag to
     * @param tag     the text of the tag we want to create
     */
    void createTag(Message message, String tag);

    /**
     * Gets the tags associated with a given message
     *
     * @param messageId The message Id we want tags for
     * @return the tags associated with the message
     */
    @Nonnull
    String[] getTags(MessageId messageId);

    /**
     * Deletes provided message.
     *
     * @param message to delete
     */
    void delete(Message message);

    /**
     * Deletes a tag from a given message
     *
     * @param message the message to remove the tag from
     * @param tag     the tag ro remove
     */
    void deleteTag(Message message, String tag);

    /**
     * @param id {@link Message#getId()}
     * @return resolved message
     */
    @Nonnull
    Message getById(final int id);

    /**
     * Finds all messages for provided tag.
     *
     * @param tag                     for which tag
     * @param messagesMappingConsumer the function to apply to each matching record
     */
    void getByTag(final String tag, final Consumer<MessageId> messagesMappingConsumer);

    /**
     * @param tag {@link Message#getTags()}
     * @return count of messages which are waiting for processing
     */
    int getMessagesForConsumingCount(final String tag);

}
