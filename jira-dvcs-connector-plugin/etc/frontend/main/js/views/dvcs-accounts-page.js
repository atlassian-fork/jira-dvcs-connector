/**
 * @module represent the dvcs accounts page
 * either render connected accounts or the no-connected-account state
 */
define('jira-dvcs-connector/bitbucket/views/dvcs-accounts-page', [
    'jquery',
    'jira-dvcs-connector/lib/backbone',
    'jira-dvcs-connector/bitbucket/models/dvcs-account-model',
    'jira-dvcs-connector/bitbucket/views/dvcs-account-view'
], function ($,
             Backbone,
             DVCSAccountModel,
             DVCSAccountView) {
    "use strict";

    return Backbone.View.extend({

        orgSelector: '.dvcs-orgdata-container',
        orgList: '#organization-list',

        initialize: function () {
            this.collection = new Backbone.Collection();
            this._createOrgViews();
            this._adjustOrgListBorders();
            this._parseGlobalSettings();
            this.emptyTemplate = dvcs.connector.plugin.soy.dvcs.accounts.noAccounts;
        },

        render: function () {
            if (this.collection.length == 0) {
                this.$el.html(this.emptyTemplate(this.globalSettings));
            }
            this._adjustOrgListBorders();
        },

        _createOrgViews: function () {
            var self = this;
            this.views = {};
            $(this.orgSelector).each(function () {
                var $this = $(this);
                var accountPanelId = $this.attr("id");
                var modelId = $this.data('org-id');
                var dvcsAccountModel = new DVCSAccountModel({
                    id: modelId,
                    name: $this.data('org-name'),
                    integrated: $this.hasClass("integrated-account"),
                    approvalState: $this.data('approval-state'),
                    hasLinkedRepositories: $this.data('has-linked-repos'),
                    smartCommitsDefault: $this.data('smart-commits-default'),
                    reposDefaultEnabled: $this.data('repos-default-enabled')
                });
                self.collection.add(dvcsAccountModel);
                var dvcsAccount = new DVCSAccountView({
                    el: '#' + accountPanelId,
                    model: dvcsAccountModel
                });
                dvcsAccount.render();
                dvcsAccount.on('jira-dvcs-connector:org-deleted', self._removeOrg, self);
                self.views[modelId] = dvcsAccount;
            });
        },

        _removeOrg: function (org) {
            this.collection.remove(org.id);
            delete this.views[org.id];
            this.render();
        },

        _parseGlobalSettings: function () {
            var hiddenDiv = this.$el.find('#dvcs-connect-source');
            this.globalSettings = {
                bitbucketOverrideUrl: hiddenDiv.data('bitbucket-override-url'),
                allSyncDisabled: hiddenDiv.data('all-sync-disabled'),
                aciEnabled: hiddenDiv.data('aci-enabled'),
                onDemandLicense: hiddenDiv.data('on-demand-license'),
                source: hiddenDiv.data('source'),
                showFeatureDiscoveryForUser: hiddenDiv.data('show-feature-discovery'),
                bitbucketRebrandEnabled: hiddenDiv.data('bbrebrand')
            };
        },

        // we display surrounding borders, indentation and expansion only when we have multiple accounts connected
        _adjustOrgListBorders: function () {
            if (this.collection.length > 1) {
                $(this.orgList).addClass('borders');
            } else {
                $(this.orgList).removeClass('borders');
                if (this.collection.length == 1) {
                    var dvcsAccountView = this.views[this.collection.first().getId()];
                    if (dvcsAccountView) {
                        dvcsAccountView.expandBody();
                        dvcsAccountView.removeExpandable();
                    }
                }
            }
        }
    });
});
