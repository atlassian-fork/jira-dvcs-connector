define('jira-dvcs-connector/bitbucket/views/dvcs-post-commit-hook-dialog', [
    'jira-dvcs-connector/lib/backbone'
], function (Backbone) {
    "use strict";

    return Backbone.View.extend({
        initialize: function (options) {
            var enable = options.enable;
            var callBackUrl = options.callBackUrl;
            var popup = new AJS.Dialog({
                width: options.width,
                height: options.height,
                id: "dvcs-postcommit-hook-registration-dialog",
                closeOnOutsideClick: false
            });

            this.popup = popup;
            popup.addHeader(enable ? AJS.I18n.getText("add.organization.repo.linking")
                : AJS.I18n.getText("add.organization.repo.unlinking"));
            popup.addPanel("Registration", dvcs.connector.plugin.soy.postCommitHookDialog({
                'registering': enable,
                'callbackUrl': callBackUrl
            }));
            popup.addButton("OK", function (dialog) {
                dialog.remove();
            }, "aui-button submit");
        },

        render: function () {
            this.popup.show();
            this.popup.updateHeight();
        }
    })
});