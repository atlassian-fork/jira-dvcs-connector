/**
 * @module jira-dvcs-connector/bitbucket/models/dvcs-repo-model collection with some filter functions
 */
define('jira-dvcs-connector/bitbucket/models/dvcs-repo-collection', [
    'jira-dvcs-connector/lib/backbone',
    'jira-dvcs-connector/bitbucket/models/dvcs-repo-model'
], function(
    Backbone,
    RepoModel
) {
    "use strict";

    var  RepoCollection = Backbone.Collection.extend({
        model: RepoModel,

        filterEnabled: function() {
            return new RepoCollection(this.where({linked: true}));
        },

        filterDisabled: function() {
            return new RepoCollection(this.where({linked: false}));
        },

        comparator: function(model) {
            return model.getName().toLowerCase();
        },

        hasEnabledRepos: function() {
            return !!this.findWhere({linked:true});
        }
    });

    return RepoCollection;
});
