/**
 * Binds events to the buttons for approve / remove connection for pending organizations which should be supplied via
 * new PendingOrganization({el: element});
 *
 * @module jira-dvcs-connector/bitbucket/views/bitbucket-admin-page-pending-org-view
 */
define('jira-dvcs-connector/bitbucket/views/bitbucket-admin-page-pending-org-view', [
    'jira-dvcs-connector/lib/backbone',
    'jira-dvcs-connector/rest/dvcs-connector-rest-client',
    'wrm/context-path',
    'jira-dvcs-connector/admin/configure-organization',
    'jira-dvcs-connector/analytics/analytics-client'
], function (
    Backbone,
    DvcsConnectorRestClient,
    contextPath,
    ConfigureOrganization,
    AnalyticsClient
) {
    "use strict";

    var dvcsConnectorRestClient = new DvcsConnectorRestClient(contextPath());
    var analyticsClient = new AnalyticsClient();

    return Backbone.View.extend({
        events: {
            "click .approve-org-button": "_handleOrgApproved",
            "click .remove-connection-button": "_handleOrgRemoved",
            "click .start-again-button": "_handleStartAgain",
            "click .remove-already-removed-org-button": "_handleRemoveAlreadyRemovedOrg"
        },

        initialize: function (element) {
            this.orgId = this.$el.data('org-id');
            this.orgName = this.$el.data('org-name');
            this.bitbucketOverrideUrl = this.$el.data('bitbucket-override-url');
            this.$approveOrgSection = this.$('.approve-org-section');
            this.$orgNotFoundSection = this.$('.org-not-found-section');
            this.$unknownErrorSection = this.$('.unknown-error-section');
            this.$confirmationSpinners = this.$('.confirmation-spinner');
            this.$auiButtons = this.$('.aui-button');
        },

        _handleOrgApproved: function (event) {
            event.preventDefault();
            var self = this;
            this._disableButtonsAndShowSpinner();
            dvcsConnectorRestClient.organization.approve(this.orgId, dvcsConnectorRestClient.organization.ApprovalLocationEnum.ON_ADMIN_SCREEN)
                .done(function (data) {
                    ConfigureOrganization.doFinishConnectionFlow(self.orgId, self.orgName);
                })
                .fail(function (err, textStatus, errorThrown) {
                    if (err.status === 404) {
                        self._showOrgNotFoundSection();
                    }
                    else {
                        self._showUnknownErrorSection();
                    }
                })
                .always(function () {
                    self._enableButtonsAndShowSpinner();
                });
        },

        _handleOrgRemoved: function (event) {
            var self = this;
            event.preventDefault();
            this._disableButtonsAndShowSpinner();
            analyticsClient.firePendingOrgRemoved(this.orgId);
            dvcsConnectorRestClient.organization.remove(this.orgId)
                .done(function (data) {
                    self.trigger('jira-dvcs-connector:pending-org-deleted');
                })
                .fail(function (err, textStatus, errorThrown) {

                    // If we get a 404, assume it is already removed;
                    // If we get a timeout, assume deletion is continuing on the server so remove org anyway;
                    // Any other error, show the unknown error screen.
                    if (err.status === 404 || err.status === 0) {
                        self.trigger('jira-dvcs-connector:pending-org-deleted');
                    }
                    else {
                        self._showUnknownErrorSection();
                    }
                })
                .always(function () {
                    self._enableButtonsAndShowSpinner();
                });
        },

        _handleStartAgain: function (event) {
            event.preventDefault();
            redirectToBitbucketAuthorize(this.bitbucketOverrideUrl);
        },

        _handleRemoveAlreadyRemovedOrg: function (event) {
            event.preventDefault();
            this.trigger('jira-dvcs-connector:pending-org-deleted');
        },

        _disableButtonsAndShowSpinner: function () {
            this.$confirmationSpinners.removeClass('hidden');
            this.$auiButtons.attr({
                'aria-pressed': 'true',
                disabled: true
            });
        },

        _enableButtonsAndShowSpinner: function () {
            this.$confirmationSpinners.addClass('hidden');
            this.$auiButtons.attr({
                'aria-pressed': 'false',
                disabled: false
            });
        },

        _showUnknownErrorSection: function() {
            this.$orgNotFoundSection.addClass('hidden');
            this.$approveOrgSection.addClass('hidden');
            this.$unknownErrorSection.removeClass('hidden');
        },

        _showOrgNotFoundSection: function() {
            this.$approveOrgSection.addClass('hidden');
            this.$unknownErrorSection.addClass('hidden');
            this.$orgNotFoundSection.removeClass('hidden');
        }

    });
});

