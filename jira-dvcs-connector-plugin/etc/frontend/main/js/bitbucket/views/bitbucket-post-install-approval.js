/**
 * Encapsulates client side behaviour for "Bitbucket post install approval" action views.
 *
 * @module jira-dvcs-connector/bitbucket/views/bitbucket-post-install-approval
 */
define('jira-dvcs-connector/bitbucket/views/bitbucket-post-install-approval', ['require'], function (require) {
    "use strict";

    var $ = require('jquery');
    var Backbone = require('backbone');
    var DvcsConnectorRestClient = require('jira-dvcs-connector/rest/dvcs-connector-rest-client');
    var Navigate = require('jira-dvcs-connector/util/navigate');

    return {
        approvalView: Backbone.View.extend({
            el: "#bb-post-install-admin-approval-dialog",
            initialize: function () {
                var $el = this.$el;
                var redirectUrl = $el.data("redirect-url");
                if (redirectUrl) {
                    Navigate.navigate(redirectUrl);
                } else {
                    var baseUrl = $el.data("base-url");
                    var orgId = $el.data("org-id");
                    var grantAccessButtonRedirectUrl = $el.data("grant-access-button-redirect-url");
                    var atlToken = $el.data("atl-token");

                    var restClient = new DvcsConnectorRestClient(baseUrl, atlToken);

                    $el.find("#approve-button").on('click', function (e) {
                        e.preventDefault();

                        restClient.organization.approve(orgId, restClient.organization.ApprovalLocationEnum.DURING_INSTALLATION_FLOW)
                            .done(function (data) {
                                Navigate.navigate(grantAccessButtonRedirectUrl);
                            }).fail(function (err, textStatus, errorThrown) {
                            if (err.status === 401) {
                                Navigate.redirectToLogin();
                            } else if (err.status === 403) {
                                Navigate.reload();
                            } else if (err.status === 404) {
                                Navigate.navigate(Navigate.getUrl() + "&approvalError");
                            } else {
                                var errorMessage = encodeURIComponent(
                                    "Approve Organization AJAX call returned error. Status: " + err.status +
                                    "; textStatus: " + textStatus + "; errorThrown: " + errorThrown);
                                Navigate.navigate(
                                    Navigate.getUrl() + "&unknownError&unknownErrorMessage=" + errorMessage);
                            }
                        });
                    });
                }
            }
        })
    };
});

