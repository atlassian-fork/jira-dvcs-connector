/**
 * A simple AMD module for accessing the console where its available.
 *
 * Provides a polyfill that is safe to use when no console is available.
 *
 * @module jira-dvcs-connector/util/console
 */
define('jira-dvcs-connector/util/console', [], function () {
    if (console) {
        return console;
    }
    var f = function () {
    };
    return {
        log: f,
        info: f,
        warn: f,
        debug: f,
        error: f
    };
});