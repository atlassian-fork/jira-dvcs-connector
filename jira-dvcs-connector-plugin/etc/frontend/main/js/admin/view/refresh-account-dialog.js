/**
 * A view that shows the 'Refreshing account' spinner dialog
 *
 * @module jira-dvcs-connector/admin/view/refresh-account-dialog
 */
define('jira-dvcs-connector/admin/view/refresh-account-dialog', ['require'], function (require) {
    'use strict';

    var Backbone = require('backbone');
    var Dialog = require('jira-dvcs-connector/aui/dialog');

    return Backbone.View.extend({

        initialize: function (options) {
            this.dialog = new Dialog({
                width: 400,
                height: 150,
                id: "refreshing-account-dialog",
                closeOnOutsideClick: false
            });
            this.dialog.addHeader('Refreshing Account');
            this.dialog.addPanel("RefreshPanel", this._renderPanelContent(options.organizationName));
        },

        show: function () {
            this.dialog.show();
            this.dialog.updateHeight();
        },

        hide: function () {
            this.dialog.remove();
        },

        _renderPanelContent: function (organizationName) {
            return "<p>Refreshing '" +
                organizationName +
                "' account. Please wait... <span class='aui-icon aui-icon-wait'>&nbsp;</span></p>";
        }

    });
});