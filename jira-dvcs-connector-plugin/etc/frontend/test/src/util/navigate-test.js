define([
    'fusion/test/qunit',
    'fusion/test/hamjest',
    'underscore'
], function (QUnit, __, _) {
    QUnit.module('jira-dvcs-connector/util/navigate', {
        require: {
            main: 'jira-dvcs-connector/util/navigate'
        },
        mocks: {
            window: QUnit.moduleMock('jira-dvcs-connector/util/window', function () {
                return {
                    location: {
                        href: "http://window.location.href/",
                        origin: "http://location.origin:1337",
                        host: "host:999",
                        protocol: "http:"
                    }
                };
            }),
            contextPath: QUnit.moduleMock('wrm/context-path', function () {
                return function () {
                    return 'contextvalue';
                }
            }),
            _: QUnit.moduleMock('underscore', function () {
                return {
                    defer: function (f) {
                        f();
                    },
                    each: function (a, f) {
                        return _.each(a, f);
                    }
                };
            })
        }
    });

    QUnit.test('Get URL returns location.href', function (assert, Navigate) {
        var url = Navigate.getUrl();

        assert.assertThat(url, __.equalTo("http://window.location.href/"));
    });

    QUnit.test('Get origin uses location.origin if available', function (assert, Navigate) {
        var url = Navigate.getOrigin();

        assert.assertThat(url, __.equalTo("http://location.origin:1337"));
    });

    QUnit.test('Get origin builds correct URL if location.origin not available', function (assert, Navigate, mocks) {
        mocks.window.location.origin = undefined;

        var url = Navigate.getOrigin();
        assert.assertThat(url, __.equalTo("http://host:999"));
    });

    QUnit.test('Get hash returns empty string if no hash', function (assert, Navigate, mocks) {
        mocks.window.location.hash = undefined;

        assert.assertThat(Navigate.getHash(), __.equalTo(""));
    });

    QUnit.test('Get hash returns hash without "#" character', function (assert, Navigate, mocks) {
        mocks.window.location.hash = '#hash';

        assert.assertThat(Navigate.getHash(), __.equalTo("hash"));
    });

    QUnit.test('Get hash returns hash without query part', function (assert, Navigate, mocks) {
        mocks.window.location.hash = '#hash?query=foo';

        assert.assertThat(Navigate.getHash(), __.equalTo("hash"));
    });

    QUnit.test('Get hash params returns empty map if no hash', function (assert, Navigate, mocks) {
        mocks.window.location.hash = undefined;

        assert.assertThat(Navigate.getHashParams(), __.equalTo({}));
    });

    QUnit.test('Get hash params returns empty map if no hash query', function (assert, Navigate, mocks) {
        mocks.window.location.hash = '#hash';

        assert.assertThat(Navigate.getHashParams(), __.equalTo({}));
    });

    QUnit.test('Get hash params returns populated map if hash params exist', function (assert, Navigate, mocks) {
        mocks.window.location.hash = '#hash?key=value&foo=bar';

        assert.assertThat(Navigate.getHashParams(), __.equalTo({"key": "value", "foo": "bar"}));
    });

    QUnit.test('Get hash params returns populated map if no hash part', function (assert, Navigate, mocks) {
        mocks.window.location.hash = '#?key=value&foo=bar';

        assert.assertThat(Navigate.getHashParams(), __.equalTo({"key": "value", "foo": "bar"}));
    });

    QUnit.test('Get hash params will return subsequent "?" characters', function (assert, Navigate, mocks) {
        mocks.window.location.hash = '#hash???key=?value?&?foo=bar';

        assert.assertThat(Navigate.getHashParams(), __.equalTo({"??key": "?value?", "?foo": "bar"}));
    });

    QUnit.test('Get hash params will ignore consecutive "&" characters', function (assert, Navigate, mocks) {
        mocks.window.location.hash = '#hash?key=value&&foo=bar';

        assert.assertThat(Navigate.getHashParams(), __.equalTo({"key": "value", "foo": "bar"}));
    });

    QUnit.test('redirectToLogin() sets window.location to correct value', function (assert, Navigate, mocks) {
        mocks.window.location.href = 'https://someurl:8080/x?a=b&c=d';

        Navigate.redirectToLogin();
        assert.assertThat(mocks.window.location, __.equalTo('contextvalue'
            + '/login.jsp?permissionViolation=true&os_destination='
            + 'https%3A%2F%2Fsomeurl%3A8080%2Fx%3Fa%3Db%26c%3Dd'));
    });

    QUnit.test('setHash(hash) adds a string after the url hash', function (assert, Navigate, mocks) {
        mocks.window.location.href = 'https://someurl:8080/';
        var hash = "some-hash";
        Navigate.setHash(hash);
        assert.assertThat(mocks.window.location.hash, __.equalTo(hash));
    });

    QUnit.test('setIdToScrollPostRefresh(id) sets the url hash as a double hash followed by the id', function (assert, Navigate, mocks) {
        mocks.window.location.href = 'https://someurl:8080/';
        var idOfElemToScrollTo = "some-id";
        Navigate.setIdToScrollPostRefresh(idOfElemToScrollTo);
        assert.assertThat(mocks.window.location.hash, __.equalTo('##' + idOfElemToScrollTo));
    });

    QUnit.test('getIdToScrollToPostRefreshIfExists() returns null if there is no such id', function (assert, Navigate, mocks) {
        var id = Navigate.getIdToScrollToPostRefreshIfExists();
        assert.assertThat(id, __.equalTo(null));
    });

    QUnit.test('getIdToScrollToPostRefreshIfExists() returns id if there are two hashes followed by a string in the url hash',
        function (assert, Navigate, mocks) {

            var id = "id-of-elem-to-scroll-to";
            mocks.window.location.hash = '##' + id;
            var result = Navigate.getIdToScrollToPostRefreshIfExists();
            assert.assertThat(result, __.equalTo(id));
        });

    QUnit.test('getIdToScrollToPostRefreshIfExists() returns null if there is only one leading hash in the url hash',
        function (assert, Navigate, mocks) {
            var id = "id-of-elem-to-scroll-to";
            mocks.window.location.hash = '#' + id;
            var result = Navigate.getIdToScrollToPostRefreshIfExists();
            assert.assertThat(result, __.equalTo(null));
        });

    QUnit.test('buildQueryParams() works on empty Object', function (assert, Navigate, mocks) {

        assert.assertThat(Navigate.buildQueryParams({}), __.equalTo(''));
    });

    QUnit.test('buildQueryParams() works on empty Object ignoring prototype values', function (assert, Navigate, mocks) {

        var paramProto = {foo: 'bah'};
        var param = {};
        param.__proto__ = paramProto;
        param.something = 1;
        assert.assertThat(Navigate.buildQueryParams(param), __.equalTo('?something=1'));
    });

    QUnit.test('buildQueryParams() works single Object', function (assert, Navigate, mocks) {

        var param = {foo: 'bah'};
        assert.assertThat(Navigate.buildQueryParams(param), __.equalTo('?foo=bah'));
    });

    QUnit.test('buildQueryParams() escapes to uri', function (assert, Navigate, mocks) {

        var param = {foo: ' '};
        assert.assertThat(Navigate.buildQueryParams(param), __.equalTo('?foo=%20'));
    });

    QUnit.test('buildQueryParams() works multiple hash', function (assert, Navigate, mocks) {

        var param = {
            foo: 'bah',
            p2: 'no',
            boo: false,
            p1: ''
        };
        assert.assertThat(Navigate.buildQueryParams(param), __.equalTo('?foo=bah&p2=no&boo=false&p1='));
    });
});
