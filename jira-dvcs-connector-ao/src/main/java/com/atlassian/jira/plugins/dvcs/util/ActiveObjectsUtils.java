package com.atlassian.jira.plugins.dvcs.util;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

public final class ActiveObjectsUtils {
    public static final String ID = "ID";
    public static final int SQL_IN_CLAUSE_MAX = Integer.getInteger("dvcs.connector.sql.in.max", 1000);
    private static final Logger log = LoggerFactory.getLogger(ActiveObjectsUtils.class);

    private ActiveObjectsUtils() {
    }

    public static <T> String renderListOperator(final String column, final String operator, final String joinWithOperator,
                                                final Iterable<T> values) {
        final StringBuilder builder = new StringBuilder(column);
        builder.append(" ").append(operator).append(" (");
        final Iterator<T> valuesIterator = values.iterator();
        int valuesInQuery = 0;
        boolean overThousandValues = false;
        while (valuesIterator.hasNext()) {
            final Object value = valuesIterator.next();
            if (value != null && StringUtils.isNotEmpty(value + "")) {
                if (valuesInQuery > 0) {
                    builder.append(", ");
                }

                builder.append("?");

                ++valuesInQuery;
                if (valuesInQuery >= SQL_IN_CLAUSE_MAX) {
                    overThousandValues = true;
                    valuesInQuery = 0;
                    builder.append(") ").append(joinWithOperator).append(" ").append(column).append(" ").append(operator).append(" (");
                }
            }
        }
        builder.append(")");
        return overThousandValues ? builder.insert(0, "(").append(")").toString() : builder.toString();
    }
}
