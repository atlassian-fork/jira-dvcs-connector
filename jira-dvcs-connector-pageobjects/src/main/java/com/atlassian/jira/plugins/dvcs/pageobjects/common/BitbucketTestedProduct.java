package com.atlassian.jira.plugins.dvcs.pageobjects.common;

import com.atlassian.jira.plugins.dvcs.pageobjects.page.BitbucketAIDLoginPage;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.BitbucketDetails;
import com.atlassian.pageobjects.DefaultProductInstance;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.binder.BrowserModule;
import com.atlassian.pageobjects.binder.InjectPageBinder;
import com.atlassian.pageobjects.binder.LoggerModule;
import com.atlassian.pageobjects.binder.StandardModule;
import com.atlassian.pageobjects.elements.ElementModule;
import com.atlassian.pageobjects.elements.timeout.TimeoutsModule;
import com.atlassian.webdriver.AtlassianWebDriverModule;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

public class BitbucketTestedProduct implements TestedProduct<WebDriverTester> {
    private static final Logger log = LoggerFactory.getLogger(BitbucketTestedProduct.class);

    private final ProductInstance productInstance;
    private final PageBinder pageBinder;
    private final WebDriverTester tester;

    public BitbucketTestedProduct(@Nonnull WebDriverTester webDriverTester) {
        this(new DefaultProductInstance(BitbucketDetails.getHostUrl(), "bitbucket", 443, "/"),
                checkNotNull(webDriverTester, "tester"));
    }

    public BitbucketTestedProduct(final ProductInstance productInstance, final WebDriverTester tester) {
        this.productInstance = productInstance;
        this.tester = tester;
        pageBinder = new InjectPageBinder(productInstance, tester,
                new AtlassianWebDriverModule(this),
                new StandardModule(this),
                new TimeoutsModule(),
                new ElementModule(),
                new BrowserModule(),
                new LoggerModule(log));
    }

    @Nonnull
    @Override
    public <P extends Page> P visit(@Nonnull Class<P> pageClass, @Nonnull Object... args) {
        return pageBinder.navigateToAndBind(pageClass, args);
    }

    @Nonnull
    @Override
    public PageBinder getPageBinder() {
        return pageBinder;
    }

    @Nonnull
    @Override
    public ProductInstance getProductInstance() {
        return productInstance;
    }

    @Nonnull
    @Override
    public WebDriverTester getTester() {
        return tester;
    }

    @Nonnull
    public BitbucketTestedProduct login(String account, String password) {
        // Ensure noone is logged in before attempting to login again
        logout();

        visit(BitbucketAIDLoginPage.class).doLogin(account, password);

        return this;
    }

    @Nonnull
    public <P extends Page> P loginAndGoTo(String account, String password, Class<P> targetPage, Object... args) {
        login(account, password);
        return visit(targetPage, args);
    }

    @Nonnull
    public BitbucketTestedProduct logout() {
        visit(BitbucketAIDLoginPage.class).doLogout();

        return this;
    }

    @Nonnull
    public <P extends Page> P waitFor(@Nonnull Class<P> pageClass, Object... args) {
        return pageBinder.delayedBind(pageClass, args).waitUntil().get();
    }
}
