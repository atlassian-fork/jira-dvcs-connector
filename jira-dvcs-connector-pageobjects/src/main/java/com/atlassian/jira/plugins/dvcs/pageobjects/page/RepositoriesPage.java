package com.atlassian.jira.plugins.dvcs.pageobjects.page;

import com.atlassian.jira.plugins.dvcs.pageobjects.component.OrganizationDiv;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.DvcsAccountsPage;
import com.atlassian.jira.plugins.dvcs.pageobjects.util.PageElementUtils;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.SelectElement;
import com.atlassian.pageobjects.elements.query.Poller;
import org.hamcrest.Matcher;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.plugins.dvcs.pageobjects.util.AciPageObjectUtils.addBitbucketAsDvcsType;
import static com.atlassian.jira.plugins.dvcs.pageobjects.util.AciPageObjectUtils.oldFlowAddOrgButton;
import static com.atlassian.pageobjects.elements.timeout.TimeoutType.PAGE_LOAD;

/**
 * Represents the page to link repositories to projects
 *
 * @deprecated use {@link DvcsAccountsPage} instead. Any functionality missing from {@link DvcsAccountsPage} should be moved
 * to that other page from here.
 */
@Deprecated
public class RepositoriesPage implements Page {
    @ElementBy(xpath = "//button[contains(concat(' ', @class , ' '),' button-panel-submit-button ') and text()='Continue']")
    PageElement continueAddOrgButton;
    /**
     * Logger for this class.
     */
    private Logger logger = LoggerFactory.getLogger(RepositoriesPage.class);
    @Inject
    private PageBinder pageBinder;
    @Inject
    private PageElementFinder elementFinder;
    /**
     * Injected {@link WebDriver} dependency.
     */
    @Inject
    private WebDriver webDriver;
    @ElementBy(id = "aui-message-bar")
    private PageElement messageBarDiv;
    @ElementBy(id = "organization-list", timeoutType = PAGE_LOAD)
    private PageElement organizationsElement;
    @ElementBy(id = "repoEntry")
    private PageElement repoEntry;
    @ElementBy(id = "urlSelect")
    private SelectElement dvcsTypeSelect;
    @ElementBy(id = "organization")
    private PageElement organization;
    @ElementBy(id = "autoLinking")
    private PageElement autoLinkNewRepos;
    @ElementBy(id = "linkRepositoryButton")
    private PageElement linkRepositoryButton;
    @ElementBy(id = "linkGithubAccountButton")
    private PageElement linkGithubAccountButton;
    @ElementBy(className = "button-panel-submit-button")
    private PageElement addOrgButton;
    // ------------------ BB ------------------------
    @ElementBy(id = "oauthBbClientId")
    private PageElement oauthBbClientId;
    @ElementBy(id = "oauthBbSecret")
    private PageElement oauthBbSecret;
    // ------------------ GH ------------------------
    @ElementBy(id = "oauthClientId")
    private PageElement oauthClientId;
    @ElementBy(id = "oauthSecret")
    private PageElement oauthSecret;
    // ------------------ GHE ------------------------
    @ElementBy(id = "urlGhe")
    private PageElement urlGhe;
    @ElementBy(id = "oauthClientIdGhe")
    private PageElement oauthClientIdGhe;
    @ElementBy(id = "oauthSecretGhe")
    private PageElement oauthSecretGhe;

    @Override
    public String getUrl() {
        return "/secure/admin/ConfigureDvcsOrganizations!default.jspa";
    }

    public void addOrganisation(
            int accountType,
            String accountName,
            String url,
            OAuthCredentials oAuthCredentials,
            boolean autoSync,
            boolean aciEnabled) {
        PageElement oldFlowAddOrgButton = oldFlowAddOrgButton(linkRepositoryButton, linkGithubAccountButton, aciEnabled);
        oldFlowAddOrgButton.click();
        waitFormBecomeVisible();
        if (aciEnabled) {
            addBitbucketAsDvcsType(dvcsTypeSelect);
        }
        dvcsTypeSelect.select(dvcsTypeSelect.getAllOptions().get(accountType));
        organization.clear().type(accountName);

        switch (accountType) {
            case 0:
                oauthBbClientId.clear().type(oAuthCredentials.key);
                oauthBbSecret.clear().type(oAuthCredentials.secret);
                break;
            case 1:
                oauthClientId.clear().type(oAuthCredentials.key);
                oauthSecret.clear().type(oAuthCredentials.secret);
                break;
            case 2:
                urlGhe.clear().type(url);
                oauthClientIdGhe.clear().type(oAuthCredentials.key);
                oauthSecretGhe.clear().type(oAuthCredentials.secret);
                break;
            default:
                break;
        }

        autoLinkNewRepos.click();
        addOrgButton.click();

        // dismiss any information alert
        try {
            webDriver.switchTo().alert().accept();
        } catch (NoAlertPresentException e) {
            // nothing to do
        }
    }

    /**
     * Returns the Organization with the given type and name.
     *
     * @param organizationType the org type
     * @param organizationName the org name
     * @return null if none is found
     */
    @Nullable
    public OrganizationDiv getOrganization(@Nonnull final String organizationType, @Nonnull final String organizationName) {
        return getOrganizations().stream()
                .filter(org -> organizationType.equals(org.getOrganizationType()))
                .filter(org -> organizationName.equals(org.getOrganizationName()))
                .findFirst()
                .orElse(null);
    }

    public List<OrganizationDiv> getOrganizations() {
        final List<OrganizationDiv> list = new ArrayList<>();
        for (PageElement orgContainer : organizationsElement.findAll(By.className("dvcs-orgdata-container"))) {
            Poller.waitUntilTrue(orgContainer.find(By.className("dvcs-org-container")).timed().isVisible());
            list.add(pageBinder.bind(OrganizationDiv.class, orgContainer));
        }
        return list;
    }

    protected void waitFormBecomeVisible() {
        Poller.waitUntilTrue("Expected add repository form to be visible", repoEntry.timed().isVisible());
    }

    /**
     * The current error status message
     *
     * @return error status message
     */

    public String getErrorStatusMessage() {
        // accessing tag name as workaround for permission denied to access property 'nr@context' issue
        PageElementUtils.permissionDeniedWorkAround(messageBarDiv);

        return messageBarDiv.find(By.className("aui-message-error")).timed().getText().by(1000l);
    }
}
