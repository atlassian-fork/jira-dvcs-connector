package com.atlassian.jira.plugins.dvcs.pageobjects.page;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.project.DeleteProjectPage;
import com.atlassian.jira.pageobjects.project.ViewProjectsPage;
import com.atlassian.jira.pageobjects.project.summary.ProjectSummaryPageTab;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.Account;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.AccountRepository;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.DvcsAccountsPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;

public class JiraPageUtils {
    private JiraPageUtils() {
    }


    public static boolean projectExists(JiraTestedProduct jira, String projectName) {
        ViewProjectsPage viewProjectsPage = jira.getPageBinder().navigateToAndBind(ViewProjectsPage.class);

        return viewProjectsPage.isRowPresent(projectName);
    }

    public static void deleteProject(JiraTestedProduct jira, String projectKey) {
        ProjectSummaryPageTab projectSummaryPageTab =
                jira.getPageBinder().navigateToAndBind(ProjectSummaryPageTab.class, projectKey);
        long projectId = projectSummaryPageTab.getProjectId();

        jira.getPageBinder().navigateToAndBind(DeleteProjectPage.class, projectId).submitConfirm();
    }

    public static void checkSyncProcessSuccess(PageBinder pageBinder) {
        waitUntilFalse(isSyncing(pageBinder));
    }

    private static TimedCondition isSyncing(PageBinder pageBinder) {
        final DvcsAccountsPage accountsPage = pageBinder.bind(DvcsAccountsPage.class);

        return accountsPage.getAccounts()
                .stream()
                .map(Account::getEnabledRepositories)
                .flatMap(List::stream)
                .map(AccountRepository::isSyncing)
                .reduce(Conditions::or)
                .get();
    }
}
