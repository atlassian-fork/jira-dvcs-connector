package com.atlassian.jira.plugins.dvcs.service.message;

/**
 * Represents key of a message.
 *
 * @param <P> type of message payload
 */
public interface MessageAddress<P extends HasProgress> {

    /**
     * @return identity of key
     */
    String getId();

    /**
     * @return type of payload
     */
    Class<P> getPayloadType();
}
