package com.atlassian.jira.plugins.dvcs.model;

import java.util.Date;
import java.util.Objects;

public class MessageQueueItem {

    private int id;
    private Date lastFailed;
    private Message message;
    private String queue;
    private int retryCount;
    private String state;
    private String stateInfo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(final Message message) {
        this.message = message;
    }

    public String getQueue() {
        return queue;
    }

    public void setQueue(final String queue) {
        this.queue = queue;
    }

    public String getState() {
        return state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public Date getLastFailed() {
        return lastFailed;
    }

    public void setLastFailed(final Date lastFailed) {
        if (Objects.nonNull(lastFailed)) {
            this.lastFailed = new Date(lastFailed.getTime());
        }
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public String getStateInfo() {
        return stateInfo;
    }

    public void setStateInfo(final String stateInfo) {
        this.stateInfo = stateInfo;
    }
}
