package com.atlassian.jira.plugins.dvcs.analytics.smartcommits;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsType;
import com.atlassian.jira.plugins.dvcs.analytics.smartcommits.event.SmartCommitEnabledByDefaultConfigEvent;
import com.atlassian.jira.plugins.dvcs.analytics.smartcommits.event.SmartCommitRepoConfigChangedEvent;
import com.google.common.base.Preconditions;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class SmartCommitsAnalyticsServiceImpl implements SmartCommitsAnalyticsService {
    private final EventPublisher eventPublisher;

    @Inject
    public SmartCommitsAnalyticsServiceImpl(final EventPublisher eventPublisher) {
        Preconditions.checkNotNull(eventPublisher);
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void fireNewOrganizationAddedWithSmartCommits(final DvcsType dvcsType, final boolean smartCommitsEnabled) {
        if (smartCommitsEnabled) {
            eventPublisher.publish(new AccountAddedWithSmartCommitsEvent(dvcsType));
        }
    }

    @Override
    public void fireSmartCommitAutoEnabledConfigChange(final int orgId, final boolean smartCommitsEnabled) {
        eventPublisher.publish(new SmartCommitEnabledByDefaultConfigEvent(orgId, smartCommitsEnabled));
    }

    @Override
    public void fireSmartCommitPerRepoConfigChange(final int repoId, final boolean smartCommitsEnabled) {
        eventPublisher.publish(new SmartCommitRepoConfigChangedEvent(repoId, smartCommitsEnabled));
    }
}
