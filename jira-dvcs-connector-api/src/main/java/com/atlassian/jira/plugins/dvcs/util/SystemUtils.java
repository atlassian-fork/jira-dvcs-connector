package com.atlassian.jira.plugins.dvcs.util;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static java.util.Collections.emptySet;
import static java.util.Optional.empty;
import static java.util.Optional.of;

public class SystemUtils {
    private static final boolean SAFE_REDIRECT_EXISTS;
    private static final boolean URL_VALIDATOR_EXISTS;
    private static final boolean GET_ALL_ISSUE_KEYS_EXISTS;
    private static final boolean GET_ALL_PROJECT_KEYS_EXISTS;

    static {
        SAFE_REDIRECT_EXISTS = getRedirectExists();
        URL_VALIDATOR_EXISTS = getUrlValidatorExists();
        GET_ALL_ISSUE_KEYS_EXISTS = getAllIssueKeysExists();
        GET_ALL_PROJECT_KEYS_EXISTS = getAllProjectKeysExists();
    }

    private static boolean getRedirectExists() {
        // Detect whether getRedirect(String, boolean) is present
        return getMethod(JiraWebActionSupport.class, "getRedirect", String.class, boolean.class).isPresent();
    }

    private static boolean getUrlValidatorExists() {
        return getMethod("com.atlassian.jira.util.UrlValidator", "isValid", String.class).isPresent();
    }

    private static boolean getAllIssueKeysExists() {
        // Detect whether getAllIssueKeys(Long) is present
        return getMethod(IssueManager.class, "getAllIssueKeys", Long.class).isPresent();
    }

    private static boolean getAllProjectKeysExists() {
        // Detect whether getAllProjectKeys(Long) is present
        return getMethod(ProjectManager.class, "getAllProjectKeys", Long.class).isPresent();
    }

    private static Optional<Method> getMethod(final Class<?> clazz, final String method, final Class<?>... parameterTypes) {
        try {
            return of(clazz.getMethod(method, parameterTypes));
        } catch (NoSuchMethodException e) {
            return empty();
        }
    }

    private static Optional<Method> getMethod(final String className, final String methodName, final Class<?>... parameterTypes) {
        try {
            Class<?> clazz = Class.forName(className);
            return getMethod(clazz, methodName, parameterTypes);
        } catch (ClassNotFoundException e) {
            return empty();
        }
    }

    public static String getRedirect(final JiraWebActionSupport action, final String url, final boolean unsafe) {
        if (SAFE_REDIRECT_EXISTS) {
            return action.getRedirect(url, unsafe);
        } else {
            return action.getRedirect(url);
        }
    }

    public static boolean isValid(final String url) {
        if (URL_VALIDATOR_EXISTS) {
            try {
                //return com.atlassian.jira.util.UrlValidator.isValid(url);
                return (boolean) getMethod("com.atlassian.jira.util.UrlValidator", "isValid", String.class).get().invoke(null, url);
            } catch (IllegalAccessException | InvocationTargetException e) {
                // Fall through and use the default validation method
            }
        }

        try {
            new URL(url);
        } catch (MalformedURLException e) {
            return false;
        }
        return true;
    }


    @SuppressWarnings("deprecation")
    public static Set<String> getAllIssueKeys(IssueManager issueManager, ChangeHistoryManager changeHistoryManager, Issue issue) {
        if (GET_ALL_ISSUE_KEYS_EXISTS) {
            return issueManager.getAllIssueKeys(issue.getId());
        } else {
            Set<String> allIssueKeys = new HashSet<>();
            if (issue != null) {
                // adding current issue key
                allIssueKeys.add(issue.getKey());
                // Adding previous issue keys
                allIssueKeys.addAll(changeHistoryManager.getPreviousIssueKeys(issue.getId()));
            }
            return allIssueKeys;
        }
    }

    public static Set<String> getAllProjectKeys(ProjectManager projectManager, Project project) {
        if (project == null) {
            return emptySet();
        }

        if (GET_ALL_PROJECT_KEYS_EXISTS) {
            return projectManager.getAllProjectKeys(project.getId());
        } else {
            return Collections.singleton(project.getKey());
        }
    }
}
