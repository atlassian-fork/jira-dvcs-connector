package com.atlassian.jira.plugins.dvcs.model.credential;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Credentials that allow use of the ACI JWT-based authentication
 */
public class PrincipalIDCredential implements Credential {
    private final String principalId;

    public PrincipalIDCredential(@Nonnull final String principalId) {
        checkArgument(isNotBlank(principalId));
        this.principalId = principalId;
    }

    public static CredentialVisitor<Optional<PrincipalIDCredential>> visitor() {
        return new PrincipalIDCredentialVisitor();
    }

    public String getPrincipalId() {
        return principalId;
    }

    @Override
    @Nonnull
    public CredentialType getType() {
        return CredentialType.PRINCIPAL_ID;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PrincipalIDCredential that = (PrincipalIDCredential) o;
        return Objects.equals(principalId, that.principalId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(principalId);
    }

    public <T> T accept(CredentialVisitor<T> visitor) {
        return visitor.visit(this);
    }

    private static class PrincipalIDCredentialVisitor extends AbstractOptionalCredentialVisitor<PrincipalIDCredential> {
        @Override
        public Optional<PrincipalIDCredential> visit(final PrincipalIDCredential credential) {
            return Optional.ofNullable(credential);
        }
    }
}
