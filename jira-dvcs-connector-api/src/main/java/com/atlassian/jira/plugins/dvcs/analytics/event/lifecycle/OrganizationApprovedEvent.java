package com.atlassian.jira.plugins.dvcs.analytics.event.lifecycle;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService.OrganizationApprovalLocation;
import com.atlassian.jira.plugins.dvcs.analytics.event.connect.ConnectOrganizationBaseEvent;
import com.atlassian.jira.plugins.dvcs.model.Organization;

import javax.annotation.ParametersAreNonnullByDefault;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Event fired when an organization is transitioned from PENDING to APPROVED
 */
@ParametersAreNonnullByDefault
@EventName("jira.dvcsconnector.organization.approved")
public class OrganizationApprovedEvent extends ConnectOrganizationBaseEvent {

    private final OrganizationApprovalLocation organizationApprovalLocation;

    public OrganizationApprovedEvent(final Organization org,
                                     final OrganizationApprovalLocation organizationApprovalLocation) {
        super(org);
        this.organizationApprovalLocation = checkNotNull(organizationApprovalLocation,
                "Location where an Organization was approved must be specified");
    }

    public OrganizationApprovalLocation getOrganizationApprovalLocation() {
        return organizationApprovalLocation;
    }
}
