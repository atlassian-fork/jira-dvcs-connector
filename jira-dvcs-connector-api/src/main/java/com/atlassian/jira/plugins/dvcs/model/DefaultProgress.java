package com.atlassian.jira.plugins.dvcs.model;

import com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag;
import org.apache.commons.lang3.time.StopWatch;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Date;
import java.util.EnumSet;
import java.util.concurrent.TimeoutException;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Default implementation of {@link Progress} that can be serialized directly for inclusion in REST responses etc.
 * <p>
 * Notes on concurrency:
 * <ol>
 * <li>Calling {@link #waitForFinish()} will block until another thread has invoked {@link #finish()}</li>
 * <li>Calling {@link #finish()} sets the value of <code>finished</code> and unblocks waiting threads</li>
 * <li>Because of the need to work in a cluster environment, {@link #waitForFinish()} is implemented using
 * polling rather than locks</li>
 * </ol>
 */
@XmlRootElement(name = "sync")
@XmlAccessorType(XmlAccessType.FIELD)
public class DefaultProgress implements Progress, Serializable {
    private static final long POLLING_FREQUENCY_MS = 500;

    @XmlAttribute
    private boolean finished = false;

    @XmlAttribute
    private int changesetCount = 0;

    @XmlAttribute
    private int jiraCount = 0;

    @XmlAttribute
    private int pullRequestActivityCount = 0;

    @XmlAttribute
    private int synchroErrorCount = 0;

    @XmlAttribute
    private Long startTime;

    @XmlAttribute
    private Long finishTime;

    @XmlAttribute
    private String error;

    @XmlAttribute
    private String errorTitle;

    @XmlAttribute
    private Date firstMessageTime;

    @XmlAttribute
    private int flightTimeMs;

    @XmlAttribute
    private int numRequests;

    @XmlAttribute
    private boolean softsync;

    @XmlAttribute
    private boolean webHookSync;

    @XmlAttribute
    private boolean warning;

    @XmlAttribute
    private boolean hasAdminPermission = true;

    @XmlTransient
    @Deprecated
    // to be removed
    private boolean shouldStop = false;

    @XmlTransient
    private int auditLogId;

    @XmlTransient
    private EnumSet<SynchronizationFlag> runAgain;

    public DefaultProgress() {
        super();
    }

    @Override
    // TODO remove synchroErrorCount
    public void inProgress(int changesetCount, int jiraCount, int synchroErrorCount) {
        this.changesetCount = changesetCount;
        this.jiraCount = jiraCount;
        this.synchroErrorCount = synchroErrorCount;
    }

    @Override
    public void inPullRequestProgress(int pullRequestActivityCount, int jiraCount) {
        this.pullRequestActivityCount = pullRequestActivityCount;
        this.jiraCount = jiraCount;
    }

    public void queued() {
        // not used, maybe one day we can have special icon for this state
    }

    public void start() {
        startTime = System.currentTimeMillis();
    }

    @Override
    public void finish() {
        finishTime = System.currentTimeMillis();
        finished = true;
    }

    @Override
    public int getChangesetCount() {
        return changesetCount;
    }

    public void setChangesetCount(int changesetCount) {
        this.changesetCount = changesetCount;
    }

    @Override
    public int getJiraCount() {
        return jiraCount;
    }

    public void setJiraCount(int jiraCount) {
        this.jiraCount = jiraCount;
    }

    @Override
    public int getPullRequestActivityCount() {
        return pullRequestActivityCount;
    }

    public void setPullRequestActivityCount(int pullRequestActivityCount) {
        this.pullRequestActivityCount = pullRequestActivityCount;
    }

    @Override
    public int getSynchroErrorCount() {
        return synchroErrorCount;
    }

    public void setSynchroErrorCount(int synchroErrorCount) {
        this.synchroErrorCount = synchroErrorCount;
    }

    @Override
    public EnumSet<SynchronizationFlag> getRunAgainFlags() {
        return runAgain;
    }

    @Override
    public void setRunAgainFlags(final EnumSet<SynchronizationFlag> runAgain) {
        this.runAgain = runAgain;
    }

    @Override
    public String getError() {
        return error;
    }

    @Override
    public void setError(String error) {
        this.error = error;
    }

    @Override
    public boolean isFinished() {
        return finished;
    }

    @Override
    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    @Override
    public void waitForFinish() {
        try {
            waitForFinish(0);
        } catch (TimeoutException e) {
            // No timeout expected
        }
    }

    @Override
    public void waitForFinish(final long timeoutMillis) throws TimeoutException {
        checkArgument(timeoutMillis >= 0, "Timeout must be non-negative.");
        final StopWatch timer = new StopWatch();
        try {
            timer.start();
            while (!finished) {
                if (timeoutMillis > 0 && timer.getTime() > timeoutMillis) {
                    throw new TimeoutException();
                }
                Thread.sleep(POLLING_FREQUENCY_MS);
            }
        } catch (InterruptedException e) {
            // Nothing needed here
        } finally {
            timer.stop();
        }
    }

    @Override
    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    @Override
    public Long getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(long finishTime) {
        this.finishTime = finishTime;
    }

    @Override
    public Date getFirstMessageTime() {
        return this.firstMessageTime;
    }

    @Override
    public void incrementRequestCount(final Date messageTime) {
        if (this.firstMessageTime == null) {
            this.firstMessageTime = messageTime;
        }
        this.numRequests++;
    }

    @Override
    public void addFlightTimeMs(int timeMs) {
        this.flightTimeMs += timeMs;
    }

    @Override
    public int getNumRequests() {
        return this.numRequests;
    }

    @Override
    public int getFlightTimeMs() {
        return this.flightTimeMs;
    }

    @Override
    public boolean isShouldStop() {
        return shouldStop;
    }

    @Override
    public void setShouldStop(boolean shouldStop) {
        this.shouldStop = shouldStop;
    }

    @Override
    public boolean hasAdminPermission() {
        return hasAdminPermission;
    }

    @Override
    public void setAdminPermission(boolean hasAdminPermission) {
        this.hasAdminPermission = hasAdminPermission;
    }

    @Override
    public int getAuditLogId() {
        return auditLogId;
    }

    @Override
    public void setAuditLogId(int auditLogId) {
        this.auditLogId = auditLogId;
    }

    @Override
    public boolean isSoftsync() {
        return softsync;
    }

    @Override
    public void setSoftsync(final boolean softsync) {
        this.softsync = softsync;
    }

    public boolean isWebHookSync() {
        return webHookSync;
    }

    public void setWebHookSync(final boolean webHookSync) {
        this.webHookSync = webHookSync;
    }

    @Override
    public boolean isWarning() {
        return warning;
    }

    @Override
    public void setWarning(final boolean value) {
        this.warning = value;
    }

    @Override
    public String getErrorTitle() {
        return errorTitle;
    }

    @Override
    public void setErrorTitle(final String errorTitle) {
        this.errorTitle = errorTitle;
    }
}
