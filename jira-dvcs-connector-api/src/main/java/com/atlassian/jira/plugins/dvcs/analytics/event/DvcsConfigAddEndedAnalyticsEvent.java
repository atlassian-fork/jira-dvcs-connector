package com.atlassian.jira.plugins.dvcs.analytics.event;

import com.atlassian.jira.plugins.dvcs.model.Organization;

import javax.annotation.ParametersAreNullableByDefault;

import static com.atlassian.jira.plugins.dvcs.analytics.event.DvcsType.fromOrganization;
import static com.atlassian.jira.plugins.dvcs.analytics.event.Outcome.FAILED;
import static com.atlassian.jira.plugins.dvcs.analytics.event.Outcome.SUCCEEDED;

/**
 * Analytics event to indicate that an add organization process has ended.
 */
@ParametersAreNullableByDefault
public class DvcsConfigAddEndedAnalyticsEvent extends DvcsConfigAddLifecycleAnalyticsEvent {
    private Integer organizationId;
    private boolean smartCommitsEnabled;
    private boolean autoLinkEnabled;
    private AuthType authType;

    private String reason;

    public DvcsConfigAddEndedAnalyticsEvent(
            final Source source,
            final DvcsType type,
            final Outcome outcome,
            final Organization org,
            final FailureReason reason) {
        super(source, Stage.ENDED, type, outcome);
        if (reason != null) {
            this.reason = reason.toString();
        }
        if (org != null) {
            this.organizationId = org.getId();
            this.smartCommitsEnabled = org.isSmartcommitsOnNewRepos();
            this.autoLinkEnabled = org.isAutolinkNewRepos();
            this.authType = AuthType.fromCredential(org.getCredential());
        }
    }

    /**
     * Create a new event for a successful organisation creation
     *
     * @param source The source of the org creation
     * @param org    The organization that was created
     * @return The new analytics event
     */
    public static DvcsConfigAddEndedAnalyticsEvent createSucceeded(
            final Source source,
            final Organization org) {
        return new DvcsConfigAddEndedAnalyticsEvent(source, fromOrganization(org), SUCCEEDED, org, null);
    }

    /**
     * Create a new event for a failed organisation creation
     *
     * @param source The source of the org creation
     * @param type   the type of org created (github, bitbucket etc.)
     * @param reason The reason for the failure
     * @return The new analytics event
     */
    public static DvcsConfigAddEndedAnalyticsEvent createFailed(
            final Source source,
            final DvcsType type,
            final FailureReason reason) {
        return new DvcsConfigAddEndedAnalyticsEvent(source, type, FAILED, null, reason);
    }

    public String getReason() {
        return reason;
    }

    public AuthType getAuthType() {
        return authType;
    }

    public Integer getOrganizationId() {
        return organizationId;
    }

    public boolean isAutoLinkEnabled() {
        return autoLinkEnabled;
    }

    public boolean isSmartCommitsEnabled() {
        return smartCommitsEnabled;
    }
}
