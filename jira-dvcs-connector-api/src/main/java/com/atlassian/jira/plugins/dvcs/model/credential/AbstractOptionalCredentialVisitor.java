package com.atlassian.jira.plugins.dvcs.model.credential;

import java.util.Optional;

/**
 * An abstract implementation of {@link CredentialVisitor} that returns {@link Optional#EMPTY} for all {@link Credential}.
 * <p>
 * Typically implementors will be interested in a specific type of {@link Credential} and should override the relevant
 * method.
 * <p>
 * Example implementations are available in the concrete {@link Credential} implementations.
 *
 * @param <T> The type parameter for returned the {@link Optional}
 */
public class AbstractOptionalCredentialVisitor<T> implements CredentialVisitor<Optional<T>> {
    @Override
    public Optional<T> visit(final BasicAuthCredential credential) {
        return Optional.empty();
    }

    @Override
    public Optional<T> visit(final PrincipalIDCredential credential) {
        return Optional.empty();
    }

    @Override
    public Optional<T> visit(final TwoLeggedOAuthCredential credential) {
        return Optional.empty();
    }

    @Override
    public Optional<T> visit(final ThreeLeggedOAuthCredential credential) {
        return Optional.empty();
    }

    @Override
    public Optional<T> visit(final UnauthenticatedCredential credential) {
        return Optional.empty();
    }
}
