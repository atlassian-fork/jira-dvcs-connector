package com.atlassian.jira.plugins.dvcs.service;

import java.util.Set;

/**
 * @author Stanislav Dvorscak
 * @see #getIssueKeys(String...)
 */
public interface LinkedIssueService {

    /**
     * Provides way how to extract issue keys from provided source - {@link String}'s.
     *
     * @param source for extraction
     * @return extracted issues
     */
    Set<String> getIssueKeys(String... source);

}
