package com.atlassian.jira.plugins.dvcs.event;

import java.io.IOException;
import java.util.Optional;

/**
 * A decorator for a SyncEvent that is aware of its scheduled sync status and the associated repo ID
 */
public interface ContextAwareSyncEvent extends SyncEvent {

    /**
     * The ID for this event
     *
     * @return the ID number associated with this event if it is present
     */
    Optional<Integer> getId();

    /**
     * Gets the repository ID the event is associated with
     *
     * @return the repository ID
     */
    int getRepoId();

    /**
     * Serialize the wrapped event and return its json string
     *
     * @return the json string of the underlying event
     */
    String asJson() throws IOException;

    /**
     * Gets the name of the wrapped event
     *
     * @return the class name of the wrapped event
     */
    String getEventName();

    /**
     * Indicated whether or not this event was created as a result of a scheduled sync
     * (in contrast to say a webhook triggered sync )
     *
     * @return is this was created by a scheduled sync
     */
    boolean scheduledSync();

    /**
     * returns the wrapped event
     *
     * @return returns the decorated event
     */
    SyncEvent getSyncEvent();
}
