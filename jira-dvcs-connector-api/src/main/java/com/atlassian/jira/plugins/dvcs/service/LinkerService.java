package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.jira.plugins.dvcs.model.Organization;
import io.atlassian.fugue.Unit;

import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;

/**
 * This service deals with managing linkers in Bitbucket. It is responsible for both the legacy Linkers that appear
 * in the UI as well as the new Connect based linkers.
 */
public interface LinkerService {

    /**
     * Asynchronously update the connect based linker values for this organization if necessary, see
     * {@link #updateConnectLinkerValues(int)}
     *
     * @param organizationId The organization to update
     * @return A future for the background task
     * @throws RejectedExecutionException If the task cannot be scheduled
     */
    Future<Unit> updateConnectLinkerValuesAsync(int organizationId) throws RejectedExecutionException;

    /**
     * Update the connect based linker values for this organization if necessary
     *
     * @param organizationId The organization to update
     */
    Unit updateConnectLinkerValues(int organizationId);

    /**
     * Removes BB custom linkers for all repos in provided organization
     *
     * @param organization The organization with repositories populated
     */
    Unit removeLinkers(Organization organization);

    /**
     * Async way of invoking {@link #removeLinkers(Organization)}
     *
     * @param organization The organization with repositories populated
     * @return A future for the background task
     * @throws RejectedExecutionException If the task cannot be scheduled
     */
    Future<Unit> removeLinkersAsync(Organization organization) throws RejectedExecutionException;
}
