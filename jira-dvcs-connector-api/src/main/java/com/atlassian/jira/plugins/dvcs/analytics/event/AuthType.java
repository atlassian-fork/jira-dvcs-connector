package com.atlassian.jira.plugins.dvcs.analytics.event;

import com.atlassian.jira.plugins.dvcs.model.credential.Credential;

/**
 * An enumeration of authentication types for use in analytics events.
 * <p>
 * Contains defined toString representations to support event whitelisting.
 */
public enum AuthType {
    ACI_JWT("jwt"),
    THREE_LEGGED_OAUTH("3LO"),
    TWO_LEGGED_OAUTH("2LO"),
    BASIC("basic"),
    UNAUTHENTICATED("none"),
    UNKNOWN("unknown");

    private final String toString;

    AuthType(final String toString) {
        this.toString = toString;
    }

    public static AuthType fromCredential(Credential c) {
        if (c == null) {
            return UNKNOWN;
        }
        switch (c.getType()) {
            case THREE_LEGGED_OAUTH:
                return THREE_LEGGED_OAUTH;
            case TWO_LEGGED_OAUTH:
                return TWO_LEGGED_OAUTH;
            case BASIC:
                return BASIC;
            case PRINCIPAL_ID:
                return ACI_JWT;
            case UNAUTHENTICATED:
                return UNAUTHENTICATED;
            default:
                return UNKNOWN;
        }
    }

    @Override
    public String toString() {
        return toString;
    }
}
