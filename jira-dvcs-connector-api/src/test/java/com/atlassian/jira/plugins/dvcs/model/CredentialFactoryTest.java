package com.atlassian.jira.plugins.dvcs.model;

import com.atlassian.jira.plugins.dvcs.model.credential.BasicAuthCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.Credential;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.ThreeLeggedOAuthCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.TwoLeggedOAuthCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.UnauthenticatedCredential;
import org.testng.annotations.Test;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CredentialFactoryTest {
    @Test
    public void testPrincipalBasedCredential() {
        final Credential result = CredentialFactory.createPrincipalCredential("principalId");

        assertThat(result).isExactlyInstanceOf(PrincipalIDCredential.class);

        PrincipalIDCredential credential = (PrincipalIDCredential) result;
        assertEquals(credential.getPrincipalId(), "principalId");
    }

    @Test
    public void test2LOBasedCredential() {
        final Credential result = CredentialFactory.create2LOCredential("key", "secret");

        assertThat(result).isExactlyInstanceOf(TwoLeggedOAuthCredential.class);

        TwoLeggedOAuthCredential credential = (TwoLeggedOAuthCredential) result;
        assertEquals(credential.getKey(), "key");
        assertEquals(credential.getSecret(), "secret");
    }

    @Test
    public void test3LOBasedCredential() {
        final Credential result = CredentialFactory.create3LOCredential("key", "secret", "token");

        assertThat(result).isExactlyInstanceOf(ThreeLeggedOAuthCredential.class);

        ThreeLeggedOAuthCredential credential = (ThreeLeggedOAuthCredential) result;
        assertEquals(credential.getKey(), "key");
        assertEquals(credential.getSecret(), "secret");
        assertEquals(credential.getAccessToken(), "token");
    }

    @Test
    public void test3LOBasedCredentialWithBlankKeySecret() {
        final Credential result = CredentialFactory.create3LOCredential(null, "", "token");

        assertThat(result).isExactlyInstanceOf(ThreeLeggedOAuthCredential.class);

        ThreeLeggedOAuthCredential credential = (ThreeLeggedOAuthCredential) result;
        assertEquals(credential.getKey(), null);
        assertEquals(credential.getSecret(), "");
        assertEquals(credential.getAccessToken(), "token");
    }

    @Test
    public void testBasicAuthCredential() {
        final Credential result = CredentialFactory.createBasicAuthCredential("user", "pass");

        assertThat(result).isExactlyInstanceOf(BasicAuthCredential.class);

        BasicAuthCredential credential = (BasicAuthCredential) result;
        assertEquals(credential.getUsername(), "user");
        assertEquals(credential.getPassword(), "pass");
    }

    @Test
    public void testUnauthenticatedCredential() {
        final Credential result = CredentialFactory.createUnauthenticatedCredential();

        assertThat(result).isExactlyInstanceOf(UnauthenticatedCredential.class);
    }

    @Test
    public void testBuilderWithPrincipalIdSet() {
        final Credential result = CredentialFactory
                .buildCredential()
                .setUsername("user")
                .setPassword("pass")
                .setKey("key")
                .setSecret("secret")
                .setToken("token")
                .setPrincipalId("principalId")
                .build();

        assertThat(result).isExactlyInstanceOf(PrincipalIDCredential.class);

        PrincipalIDCredential credential = (PrincipalIDCredential) result;
        assertEquals(credential.getPrincipalId(), "principalId");
    }

    @Test
    public void testBuilderWithAccessTokenSet() {
        final Credential result = CredentialFactory
                .buildCredential()
                .setUsername("user")
                .setPassword("pass")
                .setKey("key")
                .setSecret("secret")
                .setToken("token")
                .build();

        assertThat(result).isExactlyInstanceOf(ThreeLeggedOAuthCredential.class);

        ThreeLeggedOAuthCredential credential = (ThreeLeggedOAuthCredential) result;
        assertEquals(credential.getKey(), "key");
        assertEquals(credential.getSecret(), "secret");
        assertEquals(credential.getAccessToken(), "token");
    }

    @Test
    public void testBuilderWithAccessTokenAndNoKeySecret() {
        final Credential result = CredentialFactory
                .buildCredential()
                .setUsername("user")
                .setPassword("pass")
                .setKey("")
                .setSecret(null)
                .setToken("token")
                .build();

        assertThat(result).isExactlyInstanceOf(ThreeLeggedOAuthCredential.class);

        ThreeLeggedOAuthCredential credential = (ThreeLeggedOAuthCredential) result;
        assertEquals(credential.getKey(), "");
        assertEquals(credential.getSecret(), null);
        assertEquals(credential.getAccessToken(), "token");
    }

    @Test
    public void testBuilderWithKeyAndSecretSet() {
        final Credential result = CredentialFactory
                .buildCredential()
                .setUsername("user")
                .setPassword("pass")
                .setKey("key")
                .setSecret("secret")
                .build();

        assertThat(result).isExactlyInstanceOf(TwoLeggedOAuthCredential.class);

        TwoLeggedOAuthCredential credential = (TwoLeggedOAuthCredential) result;
        assertEquals(credential.getKey(), "key");
        assertEquals(credential.getSecret(), "secret");
    }

    @Test
    public void testBuilderWithNoSecretSet() {
        final Credential result = CredentialFactory
                .buildCredential()
                .setUsername("user")
                .setPassword("pass")
                .setKey("key")
                .build();

        assertThat(result).isExactlyInstanceOf(BasicAuthCredential.class);

        BasicAuthCredential credential = (BasicAuthCredential) result;
        assertEquals(credential.getUsername(), "user");
        assertEquals(credential.getPassword(), "pass");
    }

    @Test
    public void testUpdateOAuthCredentialWith3LOCredentialNullKeySecret() {
        ThreeLeggedOAuthCredential credential = new ThreeLeggedOAuthCredential("key", "secret", "token");

        final Credential result = CredentialFactory.updateOAuthCredential(credential, null, null);

        assertNotNull(result);

        ThreeLeggedOAuthCredential result3LO = result.accept(ThreeLeggedOAuthCredential.visitor()).get();
        assertEquals(result3LO.getKey(), null);
        assertEquals(result3LO.getSecret(), null);
        assertEquals(result3LO.getAccessToken(), "token");
    }

    @Test
    public void testUpdateOAuthCredentialWith3LOCredentialNonNullKeySecret() {
        ThreeLeggedOAuthCredential credential = new ThreeLeggedOAuthCredential("key", "secret", "token");

        final Credential result = CredentialFactory.updateOAuthCredential(credential, "newKey", "newSecret");

        assertNotNull(result);

        ThreeLeggedOAuthCredential result3LO = result.accept(ThreeLeggedOAuthCredential.visitor()).get();
        assertEquals(result3LO.getKey(), "newKey");
        assertEquals(result3LO.getSecret(), "newSecret");
        assertEquals(result3LO.getAccessToken(), "token");
    }

    @Test
    public void testUpdateOAuthCredentialWithMinimal3LOCredentialNonNullKeySecret() {
        ThreeLeggedOAuthCredential credential = new ThreeLeggedOAuthCredential(null, "", "token");

        final Credential result = CredentialFactory.updateOAuthCredential(credential, "newKey", "newSecret");

        assertNotNull(result);

        ThreeLeggedOAuthCredential result3LO = result.accept(ThreeLeggedOAuthCredential.visitor()).get();
        assertEquals(result3LO.getKey(), "newKey");
        assertEquals(result3LO.getSecret(), "newSecret");
        assertEquals(result3LO.getAccessToken(), "token");
    }

    @Test
    public void testUpdateOAuthCredentialWithNon3LOCredentialNonNullKeySecret() {
        BasicAuthCredential credential = new BasicAuthCredential("user", "pass");

        final Credential result = CredentialFactory.updateOAuthCredential(credential, "newKey", "newSecret");

        assertNotNull(result);

        TwoLeggedOAuthCredential result2LO = result.accept(TwoLeggedOAuthCredential.visitor()).get();
        assertEquals(result2LO.getKey(), "newKey");
        assertEquals(result2LO.getSecret(), "newSecret");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testUpdateOAuthCredentialWithNon3LOCredentialNullKeySecret() {
        BasicAuthCredential credential = new BasicAuthCredential("user", "pass");

        CredentialFactory.updateOAuthCredential(credential, null, "");
    }
}
