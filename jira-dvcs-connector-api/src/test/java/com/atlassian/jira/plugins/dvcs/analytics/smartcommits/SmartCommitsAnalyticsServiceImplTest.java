package com.atlassian.jira.plugins.dvcs.analytics.smartcommits;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsType;
import com.atlassian.jira.plugins.dvcs.analytics.smartcommits.event.SmartCommitEnabledByDefaultConfigEvent;
import com.atlassian.jira.plugins.dvcs.analytics.smartcommits.event.SmartCommitRepoConfigChangedEvent;
import com.atlassian.jira.plugins.dvcs.util.MockitoTestNgListener;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@Listeners(MockitoTestNgListener.class)
public class SmartCommitsAnalyticsServiceImplTest {
    @Mock
    private EventPublisher eventPublisher;

    @InjectMocks
    private SmartCommitsAnalyticsServiceImpl classUnderTest;

    @Test
    public void testFireNewOrganizationAddedWithSmartCommitsEnabled() {
        classUnderTest.fireNewOrganizationAddedWithSmartCommits(DvcsType.BITBUCKET, true);
        verify(eventPublisher).publish(new AccountAddedWithSmartCommitsEvent(DvcsType.BITBUCKET));
    }

    @Test
    public void testFireNewOrganizationAddedWithSmartCommitsDisabled() {
        classUnderTest.fireNewOrganizationAddedWithSmartCommits(DvcsType.BITBUCKET, false);
        verifyNoMoreInteractions(eventPublisher);
    }

    @Test
    public void testFireSmartCommitAutoEnabledConfigChange() {
        classUnderTest.fireSmartCommitAutoEnabledConfigChange(1, false);
        verify(eventPublisher).publish(new SmartCommitEnabledByDefaultConfigEvent(1, false));
    }

    @Test
    public void testFireSmartCommitPerRepoConfigChange() {
        classUnderTest.fireSmartCommitPerRepoConfigChange(1, false);
        verify(eventPublisher).publish(new SmartCommitRepoConfigChangedEvent(1, false));
    }
}
