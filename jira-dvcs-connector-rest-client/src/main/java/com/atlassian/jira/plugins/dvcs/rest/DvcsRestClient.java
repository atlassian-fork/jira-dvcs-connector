package com.atlassian.jira.plugins.dvcs.rest;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.sun.jersey.api.client.WebResource;

import javax.annotation.Nonnull;
import java.net.URL;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Allows the DVCS functional tests to communicate with the DVCS plugin via REST.
 *
 * @param <T>
 */
public abstract class DvcsRestClient<T extends DvcsRestClient<T>> extends RestApiClient<T> {
    private final URL baseURL;

    protected DvcsRestClient(@Nonnull final JIRAEnvironmentData environmentData) {
        super(checkNotNull(environmentData));
        baseURL = environmentData.getBaseUrl();
    }

    @Override
    protected WebResource createResource() {
        return resourceRoot(baseURL.toString()).path("rest/bitbucket/1.0");
    }

    protected WebResource createInternalResource() {
        return resourceRoot(baseURL.toString()).path("rest/dvcs-connector-internal/1.0");
    }

    @Override
    protected WebResource resourceRoot(final String url) {
        return super.resourceRoot(url);
    }
}
