package com.atlassian.jira.plugins.dvcs.rest;

import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.RepositoryList;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.google.common.collect.ImmutableList;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

/**
 * A test-time client for the Repository domain entity.
 */
@ParametersAreNonnullByDefault
public class RepositoryClient extends DvcsRestClient<RepositoryClient> implements Serializable {

    public RepositoryClient(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    /**
     * Return all repositories across all organizations
     *
     * @return Every single repository in the DVCS connector
     */
    @Nonnull
    public List<Repository> getRepositories() {
        final RepositoryList repositoryList = publicApi()
                .path("/")
                .get(RepositoryList.class);
        return Optional.ofNullable(repositoryList)
                .map(RepositoryList::getRepositories)
                .orElse(ImmutableList.of());
    }

    /**
     * Returns the repositories for the given organization.
     *
     * @param organizationId the organization's database ID
     * @return a list of repos populated with at least the id, orgId, dvcsType, slug, name, smartcommitsEnabled,
     * repositoryUrl, and logo
     */
    @Nonnull
    public List<Repository> getRepositories(final int organizationId) {
        return publicApi()
                .path("find")
                .queryParam("orgId", String.valueOf(organizationId))
                .get(new GenericType<List<Repository>>() {
                });
    }

    /**
     * Returns the repository names for the given organization.
     *
     * @param organizationId the organization's database ID
     * @return a list of repo names
     */
    @Nonnull
    public List<String> getRepositoryNames(final int organizationId) {
        return getRepositories(organizationId).stream().map(Repository::getName).collect(toList());
    }

    public Repository getRepository(final int repositoryId) {
        return publicApi(repositoryId).get(new GenericType<Repository>() {
        });
    }

    /**
     * Find the first repository whose name matches
     *
     * @param organizationId   The organization to search in
     * @param repositoryName The name of the repository to match
     * @return Repository if it exists or empty
     */
    @Nonnull
    public Optional<Repository> findRepositoryByName(final int organizationId, final String repositoryName) {
        return getRepositories(organizationId)
                .stream()
                .filter(repository -> Objects.equals(repositoryName, repository.getName()))
                .findFirst();
    }

    /**
     * Enable/Link the given repository
     *
     * @param repositoryId The repository to sync
     */
    @Nonnull
    public void linkRepository(final int repositoryId) {
        publicApi(repositoryId)
                .path("enable")
                .post();
    }

    /**
     * Sync the given repository
     *
     * @param repositoryId The repository to sync
     */
    @Nonnull
    public void syncRepository(final int repositoryId) {
        internalApi(repositoryId)
                .path("syncAndWait")
                .post();
    }
    @Nonnull
    private WebResource publicApi() {
        return createResource().path("repository");
    }

    @Nonnull
    private WebResource publicApi(final int repositoryId) {
        return publicApi().path("" + repositoryId);
    }

    @Nonnull
    private WebResource internalApi() {
        return createInternalResource().path("repository");
    }

    @Nonnull
    private WebResource internalApi(final int repositoryId) {
        return internalApi().path("" + repositoryId);
    }

}
