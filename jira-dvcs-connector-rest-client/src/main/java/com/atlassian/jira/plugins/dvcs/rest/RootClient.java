package com.atlassian.jira.plugins.dvcs.rest;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

/**
 * A test-time client for the root resource "/".
 */
public class RootClient extends DvcsRestClient<RootClient> {
    public RootClient(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    /**
     * Reloads accounts configuration (not sure what that means, I just copied it).
     */
    public void reloadAccountsConfiguration() {
        final String responseBody = createResource()
                .path("integrated-accounts")
                .path("reloadSync")
                .get(String.class);
        assert "OK".equals(responseBody);
    }
}
